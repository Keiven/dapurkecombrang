<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'ClientController@showWelcome');
Route::get('/home', 'ClientController@GetHomeView');
Route::get('/menu','ClientController@GetProductView');
Route::get('/menu/{id}/{tittle}', 'ClientController@GetProductDetailView');
Route::get('/gallery', 'ClientController@GetGalleryView');
Route::get('/news', 'ClientController@GetNewsView');
Route::get('/news/tag/{id}','ClientController@GetNewsTagView');
Route::get('/news/{id}/{tittle}', 'ClientController@GetNewsDetailView');
Route::get('/media', 'ClientController@GetMediaView');
Route::get('/media/tag/{id}','ClientController@GetMediaTagView');
Route::get('/media/{id}/{tittle}', 'ClientController@GetMediaDetailView');
Route::get('/contact', 'ClientController@GetContactView');
Route::get('/about', 'ClientController@GetAboutView');
Route::get('/services', 'ClientController@GetServicesView');






/*================== ADMIN PANEL ==================*/
Route::get('/adm/login','AdminController@GetAdminLoginView');
Route::post('/adm/login','AdminController@GetAdminLoginAuth');
Route::get('/adm/logout','AdminController@GetAdminLogout');

Route::group(array('before' => 'auth'), function()
{
	Route::get('/adm/dashboard','AdminController@GetAdminDashboardView');
	

	Route::get('/adm/menu','AdminController@GetAdminProductView');
	Route::post('/adm/menu','AdminController@PostAdminProductView');
	Route::post('/adm/menu/banner/edit','AdminController@PostAdminProductBannerEdit');
	Route::get('/adm/menu/{id}/edit','AdminController@GetAdminProductEditView');
	Route::post('/adm/menu/{id}/edit','AdminController@PostAdminProductEditView');
	Route::get('/adm/menu/{id}/delete','AdminController@GetAdminProductDelete');

	Route::get('/adm/menu/category/add','AdminController@GetAdminProductCategoryAddView');
	Route::post('/adm/menu/category/add','AdminController@PostAdminProductCategoryAddView');
	Route::get('/adm/menu/category/edit={id}','AdminController@GetAdminProductCategoryEditView');
	Route::Post('/adm/menu/category/edit={id}','AdminController@PostAdminProductCategoryEditView');
	Route::get('/adm/menu/category/delete={id}','AdminController@GetAdminProductCategoryDelete');

	Route::get('/adm/gallery','AdminController@GetAdminGalleryView');
	Route::post('/adm/gallery','AdminController@PostAdminGalleryView');
	Route::post('/adm/gallery/banner/edit','AdminController@PostAdminGalleryBannerEdit');
	Route::get('/adm/gallery/{id}/delete','AdminController@GetAdminGalleryDelete');
	Route::get('/adm/gallery/{id}/edit','AdminController@GetAdminGalleryEdit');
	Route::post('/adm/gallery/{id}/edit','AdminController@PostAdminGalleryEdit');

	
	Route::get('/adm/slider-image','AdminController@GetAdminSliderImageView');
	Route::post('/adm/slider-image','AdminController@PostAdminSliderImageView');
	Route::get('/adm/slider-image/{id}/delete','AdminController@GetAdminSliderImageDelete');
	
	Route::get('/adm/news','AdminController@GetAdminNewsView');
	Route::post('/adm/news','AdminController@PostAdminNewsView');
	Route::get('/adm/news/{id}/delete','AdminController@GetAdminNewsDelete');
	Route::get('/adm/news/{id}/edit','AdminController@GetAdminNewsEdit');
	Route::post('/adm/news/{id}/edit','AdminController@PostAdminNewsEdit');


	Route::get('/adm/media','AdminController@GetAdminMediaView');
	Route::post('/adm/media','AdminController@PostAdminMediaView');
	Route::get('/adm/media/{id}/delete','AdminController@GetAdminMediaDelete');
	Route::get('/adm/media/{id}/edit','AdminController@GetAdminMediaEdit');
	Route::post('/adm/media/{id}/edit','AdminController@PostAdminMediaEdit');

	Route::get('/adm/user/management','AdminController@GetAdminUserManagementView');
	Route::post('/adm/user/management','AdminController@PostAdminUserManagementView');

	Route::get('/adm/user/profile/{id}','AdminController@GetAdminUserProfileView');
	Route::post('/adm/user/profile/{id}/save','AdminController@PostAdminUserProfileView');
	Route::get('/adm/user/profile/{id}/delete','AdminController@GetAdminUserProfileDelete');

	Route::get('/adm/opening-page','AdminController@GetAdminOpeningPageView');
	Route::post('/adm/opening-page','AdminController@PostAdminOpeningPageView');

	Route::get('/adm/about','AdminController@GetAdminAboutView');

	Route::post('/adm/about','AdminController@PostAdminAboutView');
	Route::get('/adm/about/partner/{id}/delete','AdminController@GetAdminAboutPartnerDelete');
	Route::post('/adm/about/partner','AdminController@PostAdminAboutPartnerView');

	Route::get('/adm/banner','AdminController@GetAdminBannerView');
	Route::post('/adm/banner','AdminController@GetAdminBannerPost');
	Route::post('/adm/banner/head','AdminController@GetAdminBannerHeaderPost');
	Route::post('/adm/banner/body','AdminController@GetAdminBannerBodyPost');
	Route::post('/adm/banner/footer','AdminController@GetAdminBannerFooterPost');

	Route::get('/adm/services','AdminController@GetAdminServicesView');
	Route::post('/adm/services','AdminController@GetAdminServicesPost');

	Route::get('/adm/setting','AdminController@GetAdminSiteSettingView');
	Route::post('/adm/setting','AdminController@PostAdminSiteSettingView');

	Route::get('/json','AdminController@json');
	Route::get('/tes','AdminController@tes');
});



//Route::get('/hash/{word}','AdminController@_Hash');