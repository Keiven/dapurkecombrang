<?php namespace App\Services;

class Embed{

	public function youtube($string) {
	    return preg_replace(
	        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
	        "<iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>",
	        $string
	    );
	}

	public function vimeo($string)
	{
		preg_match(
	        '/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/',
	        $string,
	        $matches
	    );

		$width  = 500;
		$height = 281;
	    $id = $matches[2];
	    return "<iframe src=\"https://player.vimeo.com/video/" .$id. "\" width=\"".$width."\" height=\"".$height."\" frameborder=\"0\" allowfullscreen></iframe>";
	}


	public function convert($url)
	{
		if (strpos($url, 'youtube.com/watch?v=') > 0) {
	        return $this->youtube($url);
	    } elseif (strpos($url, 'vimeo.com/') > 0) {
	        return $this->vimeo($url);
	    } else {
	        return null;
	    }
	}
}