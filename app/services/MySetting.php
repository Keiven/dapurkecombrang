<?php namespace App\Services;


Class MySetting{
	public function get()
	{
		$setting = \Setting::remember(5)->find(1);

		return $setting;
	}

	public function getDesc()
	{
		$desc 	= \Desc::remember(1)->find(1);

		return $desc;
	}
}