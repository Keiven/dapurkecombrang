<?php namespace App\Services;
use Config, File, Log;

class Image {
    /**
     * Instance of the Imagine package
     * @var Imagine\Gd\Imagine
     */
    protected $imagine;
 
    /**
     * Type of library used by the service
     * @var string
     */
    protected $library;
 
    /**
     * Initialize the image service
     * @return void
     */
    public function __construct()
    {
        if ( ! $this->imagine)
        {
            $this->library = Config::get('image.library', 'gd');
 
            // Now create the instance
            if     ($this->library == 'imagick') $this->imagine = new \Imagine\Imagick\Imagine();
            elseif ($this->library == 'gmagick') $this->imagine = new \Imagine\Gmagick\Imagine();
            elseif ($this->library == 'gd')      $this->imagine = new \Imagine\Gd\Imagine();
            else                                 $this->imagine = new \Imagine\Gd\Imagine();
        }
    }
    /**
	 * Resize an image
	 * @param  string  $url
	 * @param  integer $width
	 * @param  integer $height
	 * @param  boolean $crop
	 * @return string
	 */
    public function tes($a,$b)
    {
        echo 'tes'.$a.' '.$b;
    }

	public function mythumb($path, $filename)
	{
	    $width  = 100;
	    $height = 100;
	    $mode   = \Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;
	    $size   = new \Imagine\Image\Box($width, $height);

	    $thumbnail   = $this->imagine->open($path.'/'.$filename)->thumbnail($size, $mode);
	    $newlocation = "{$path}/thumb-{$filename}";
	    $thumbnail->save($newlocation);
	    return $newlocation;

	}

    public function thumbCustom($path, $filename, $width, $height)
    {
        $mode   = \Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;
        $size   = new \Imagine\Image\Box($width, $height);

        $thumbnail   = $this->imagine->open($path.'/'.$filename)->thumbnail($size, $mode);
        $newlocation = "{$path}/thumb-{$filename}";
        $thumbnail->save($newlocation);
        return $newlocation;

    }
}