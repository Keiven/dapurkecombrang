<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php $desc = MySetting::getDesc(); ?>
<!DOCTYPE HTML>
<html>
<head>
  @include('template.head')
</head>
<body>
<!--banner start here-->
 @include('template.banner-main', ['desc' => $desc])
<!--baner end here-->
<!--navgation start here-->
<!--baner end here-->
<!--navgation start here-->
  @include('template.navbar')



  @include('template.js')

<!--manu start here-->
<div class="menu">
    <div class="container">
        <div class="menu-main">
            <div class="menu-head">
              <h3>Our Menu</h3>
              <p>{{$desc->DescProduct}}</p>
            </div>
            <?php $i=1;?>
            @foreach($category as $_category)
            @if($i==1)
            <div class="menu-top ">
              <h3>{{$_category->CategoryName}}</h3>
                <div class="menu-top-grids mgd1 wow bounceInRight" data-wow-delay="0.5s">
            @elseif($i%2==1)
            <div class="menu-top mgd1 wow bounceInRight" data-wow-delay="0.5s">  
                <h3>{{$_category->CategoryName}}</h3>
                <div class="menu-top-grids mgd1 wow bounceInRight" data-wow-delay="0.5s">
             @elseif($i%2==0)
            <div class="menu-top mgd2 wow bounceInLeft" data-wow-delay="0.5s">  
                <h3>{{$_category->CategoryName}}</h3>
                <div class="menu-top-grids mgd1 wow bounceInLeft" data-wow-delay="0.5s">
            @endif
                @foreach($product as $_product)
                @if($_product->CategoryName != $_category->CategoryName) 
                  <?php continue; ?>
                @endif
                    <div class="col-md-4 menu-items">
                        <a href="#"><img src="{{asset($_product->ProductImage)}}" alt="{{'dapur kecombrang '.$_product->ProductName}}" class="img-responsive"></a>
                        <h4><a href="#">{{$_product->ProductName}}</a></h4>
                    </div>
                @endforeach

                  <div class="clearfix"> </div>
                </div>
            </div>
            <?php $i++;?>
            @endforeach
            <br><br>

    </div>
</div>


<!--menu end here-->
<!--footer start here-->
@include('template.footer',['desc' => $desc])
<!--footer end here-->

</body>
</html>