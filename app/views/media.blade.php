<!DOCTYPE html>
<html lang="en">
<head>
@include('template.head')
</head>

<body>
<!--==============================
              header
=================================-->
<header>
  @include('template.navbar')
</header>
<!--=====================
          Content
======================-->
<section id="content" class="gallery"><div class="ic">More Website Templates @ TemplateMonster.com - August11, 2014!</div>
  <div class="container">
    <div class="row">
      <div class="grid_12">
        <h2>Gallery</h2>
      </div>
      <div class="grid_4">
        <div class="box">
          <a href="asset/images/big1.jpg" class="gall_item"><img src="asset/images/page3_img1.jpg" alt=""><span></span></a>
          <div class="box_bot">
            <div class="box_bot_title">Bliquam nibh ante</div>
            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. In mollis erat mattis neque facilisis, sit amet ultricies erat rutrum. Cras facilisis, nulla vel viverra auctor, leo magna sodales felis, quis malesuada nibh odio ut velit</p>
            <a href="#" class="btn">more</a>
          </div>
        </div>
      </div>

      <div class="grid_4">
        <div class="box">
          <a href="asset/images/big2.jpg" class="gall_item"><img src="asset/images/page3_img2.jpg" alt=""><span></span></a>
          <div class="box_bot">
            <div class="box_bot_title">Aliquam nibh ante</div>
            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. In mollis erat mattis neque facilisis, sit amet ultricies erat rutrum. Cras facilisis, nulla vel viverra auctor, leo magna sodales felis, quis malesuada nibh odio ut velit</p>
            <a href="#" class="btn">more</a>
          </div>
        </div>
      </div>

      <div class="grid_4">
        <div class="box">
          <a href="asset/images/big3.jpg" class="gall_item"><img src="asset/images/page3_img3.jpg" alt=""><span></span></a>
          <div class="box_bot">
            <div class="box_bot_title">Cliqum nibh anteter</div>
            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. In mollis erat mattis neque facilisis, sit amet ultricies erat rutrum. Cras facilisis, nulla vel viverra auctor, leo magna sodales felis, quis malesuada nibh odio ut velit</p>
            <a href="#" class="btn">more</a>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!--==============================
              footer
=================================-->
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="grid_12"> 
        <h2>Contacts</h2>
        <div class="footer_phone">+1 800 559 6580</div>
        <a href="#" class="footer_mail">info@demolink.org</a>
        <div class="sub-copy">Website designed by <a href="http://www.templatemonster.com/" rel="nofollow">TemplateMonster.com</a></div>
      </div>
    </div>

  </div>  
</footer>
<a href="#" id="toTop" class="fa fa-chevron-up"></a>

</body>
@include('template.js')
<script>
 $(window).load(function(){
  $().UItoTop({ easingType: 'easeOutQuart' });
  $('#stuck_container').tmStickUp({}); 
    $('.gallery .gall_item').touchTouch();
 }); 

</script>
</html>

