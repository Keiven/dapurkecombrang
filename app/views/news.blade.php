<!DOCTYPE html>
<html lang="en">
<head>
@include('template.head')
</head>

<body>
<!--==============================
              header
=================================-->
<header>
  @include('template.navbar')
</header>
<!--=====================
          Content
======================-->
<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - August11, 2014!</div>
  <div class="container">
    <div class="row">
      <div class="grid_8">
        <h2>News</h2>
        @foreach($news as $_news)
        <?php $link = asset('news/'.$_news->NewsId.'/'.str_replace(' ','-',strtolower($_news->NewsTittle))) ?>
        <div class="blog">
          <img src="{{asset($_news->NewsImage)}}" alt="{{'Attaya Futsal'.' '.$_news->NewsTittle}}" class="img_inner">
          <div class="text1 color1"><a href="#">{{ucwords($_news->NewsTittle)}}</a></div>
          {{substr($_news->NewsContent,0, strpos($_news->NewsContent, "</p>"))}}
          <br>
          <a href="{{$link}}" class="btn">more</a>
          <table>
            <tbody>
              <tr>
                <td><time datetime="2014-01-01">
                  <span class="fa fa-calendar"></span>
                  {{date("M d, Y", strtotime($_news->created_at))}}
                </time> </td>
                <td><a href="#"><div class="fa fa-user"></div> {{$_news->NewsWriter}}</a></td>
                <td><a href="#"><span class="fa fa-link"></span> Permalink</a></td>
              </tr>
              <tr>
                <td><div class="fa fa-bookmark"></div> Uncategorized  </td>
                <td colspan="2"><div class="fa fa-tag"></div> 
                  <?php 
                      $tag = explode(",",$_news->NewsTags); 
                      $limit = count($tag);
                      $i=0;
                  ?>
                  @foreach($tag as $_tag)
                  @if(++$i != $limit)
                    <a href="{{asset('news/tag/'.$_tag)}}">{{ucwords($_tag).','}} </a>
                  @else
                    <a href="{{asset('news/tag/'.$_tag)}}">{{ucwords($_tag)}} </a>
                  @endif
                  @endforeach
                  @if($limit == 0)
                    No Tag
                  @endif
                </td>
              </tr>
            </tbody>
        </table>

        </div>
      </div>
       @endforeach

      
      <div class="grid_4">
        <h2>Latest News</h2>
        <ul class="list-1">
          @foreach($latest as $_latest)
          <?php $link = asset('news/'.$_latest->NewsId.'/'.str_replace(' ','-',strtolower($_latest->NewsTittle))) ?>
          <li><span></span><a href="{{$link}}"><div class="fa fa-chevron-right"></div>{{{$_latest->NewsTittle}}}</a></li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</section>
<!--==============================
              footer
=================================-->
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="grid_12"> 
        <h2>Contacts</h2>
        <div class="footer_phone">+1 800 559 6580</div>
        <a href="#" class="footer_mail">info@demolink.org</a>
        <div class="sub-copy">Website designed by <a href="http://www.templatemonster.com/" rel="nofollow">TemplateMonster.com</a></div>
      </div>
    </div>

  </div>  
</footer>
<a href="#" id="toTop" class="fa fa-chevron-up"></a>
</body>
@include('template.js')
</html>

