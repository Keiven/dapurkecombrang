<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        @include('admin.template.head')
    </head>
    <body class="bg_d">
        @include('admin.template.navbar')
            
        <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box" id="n_fileupload">
                            <div class="w-box-header">
                                <h4>Upload News</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="row-fluid">
                                <form method="post" enctype="multipart/form-data">
                                    <div class="span6">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;">
                                                <img src="{{asset('asset2/img/dummy_150x150.gif')}}" alt="">
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" name="photo"></span>
                                                <span class="help-block">Best view 770 x 347px.</span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                            <label>News Title</label>
                                            <input type="text" placeholder="Type something…" class="span8" name="title">
                                            <label>Article</label>
                                        <div class="w-box" id="n_wysiwg">
                                            <div class="w-box-content cnt_no_pad">
                                                <textarea id="wysiwg_editor" cols="30" rows="10" name="content"></textarea>
                                            </div>
                                        </div>
                                        <label>Tag</label>
                                        <input type="text" id="s2_tag_handler" class="span12" value="" name="tags">
                                        <span class="help-block">Try to enter a space or a comma after tag.</span>

                                        <div class="sepH_b">
                                             <p style="color:#F00; font-size:15px;">{{Session::get('message')}}</p>
                                            <button type="submit" class="btn">Publish</button>
                                        </div>
                                        
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Post News</h4>
                                <div class="btn-group">
                                    <a href="#" class="btn btn-inverse btn-mini delete_rows_dt" data-tableid="dt_gal" title="Edit">Delete</a>
                                    <a href="#" class="btn btn-inverse btn-mini" title="View">Another Action</a>
                                </div>
                            </div>
                            <div class="w-box-content">
                                <table class="table table-vam table-striped" id="dt_gal">
                                    <thead>
                                        <tr>
                                            <th class="table_checkbox" style="width:13px"><input type="checkbox" name="select_rows" class="select_rows" data-tableid="dt_gal" /></th>
                                            <th>Image</th>
                                            <th>News Title</th>
                                            <th>News Content</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($news as $_news)
                                        <tr>
                                            <td><input type="checkbox" name="row_sel" class="row_sel" /></td>
                                            <td style="width:60px">
                                                <a title="{{{$_news->NewsTittle}}}" class="thumbnail">
                                                    <img alt="" src="{{asset($_news->NewsImageThumb)}}" style="height:50px;width:50px">
                                                </a>
                                            </td>
                                            <td>{{{$_news->NewsTittle}}}</td>
                                            <td>{{{substr(strip_tags($_news->NewsContent),0,70).'...'}}}</td>
                                            <td>26/12/2012</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{asset('adm/news/'.$_news->NewsId.'/edit')}}" class="btn btn-mini" title="Edit"><i class="icon-pencil"></i></a>
                                                    <a href="{{asset('news/')}}" class="btn btn-mini" title="View"><i class="icon-eye-open"></i></a>
                                                    <a href="{{asset('adm/news/'.$_news->NewsId.'/delete')}}" class="btn btn-mini" title="Delete"><i class="icon-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="w-box-footer">
                                {{$news->links('admin.template.custom-paginate')}}
                            </div>
                        </div>
                        <!-- confirmation box -->
                        <div class="hide">
                            <div id="confirm_dialog" class="cbox_content">
                                <div class="sepH_c"><strong>Are you sure you want to delete this row(s)?</strong></div>
                                <div>
                                    <a href="#" class="btn btn-small btn-beoro-3 confirm_yes">Yes</a>
                                    <a href="#" class="btn btn-small confirm_no">No</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
   @include('admin.template.js')


    </body>
</html>