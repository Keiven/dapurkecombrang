<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        @include('admin.template.head')
    </head>
    <body class="bg_d">
        @include('admin.template.navbar')
            
        <!-- main content -->
            <div class="container">
                <form method="post" enctype="multipart/form-data">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box" id="n_fileupload">
                            <div class="w-box-header">
                                <h4>About Picture Upload</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;">
                                            @if($about->AboutImage == '')
                                                <img src="{{asset('asset2/img/dummy_150x150.gif')}}" alt="">
                                            @else
                                                <img src="{{asset($about->AboutImage)}}" alt="">
                                            @endif
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-small btn-file"><span class="fileupload-new">Select Image</span><span class="fileupload-exists">Change</span><input type="file" name="photo"></span>
                                                <span class="help-block">min size 400 x 300  px.</span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Edit Content</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="w-box" id="n_wysiwg">
                                            <div class="w-box-content cnt_no_pad">
                                                <textarea name="content" id="wysiwg_editor" cols="30" rows="10">{{$about->AboutContent}}</textarea>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="sepH_b">
                                            <button type="submit" class="btn">Publish</button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <br>
                

            </div>

            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
        @include('admin.template.js')

    </body>
</html>