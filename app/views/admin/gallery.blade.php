<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        @include('admin.template.head')
    </head>
    <body class="bg_d">
    <!-- main wrapper (without footer) -->    
        <div class="main-wrapper">
        <!-- top bar -->
        @include('admin.template.navbar')

        <!-- breadcrumbs -->
            <!-- <div class="container">
                <ul id="breadcrumbs">
                    <li><a href="javascript:void(0)"><i class="icon-home"></i></a></li>
                    <li><a href="javascript:void(0)">Content</a></li>
                    <li><a href="javascript:void(0)">Article: Lorem ipsum dolor...</a></li>
                    <li><a href="javascript:void(0)">Comments</a></li>
                    <li><span>Lorem ipsum dolor sit amet...</span></li>
                </ul>
            </div> -->
            
        <!-- main content -->
            <div class="container">

                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box" id="n_fileupload">
                            <div class="w-box-header">
                                <h4>Gallery Text Description</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <form method="post" action="/adm/gallery/banner/edit">
                                <div class="row-fluid">
                                    <div class="span6">
                                            <label>Description</label>
                                            <textarea rows="2" class="span8" name="desc">{{$desc->DescGallery}}</textarea>
                                        <div class="sepH_b">
                                            <p style="color:#F00; font-size:15px;">{{Session::get('message2')}}</p>
                                            <input type="submit" class="btn" value="Publish">
                                        </div>
                                        
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box" id="n_fileupload">
                            <div class="w-box-header">
                                <h4>Upload Gallery</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <form method="post" enctype="multipart/form-data">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;">
                                                <img src="{{asset('asset2/img/dummy_150x150.gif')}}" alt="">
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" name="photo"></span>
                                                <span class="help-block">Best view 360 x 260px.</span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                            <label>Gallery Title</label>
                                            <input type="text" placeholder="Type something…" class="span8" name="title"/>
                                            <label>Gallery Description</label>
                                            <textarea rows="2" class="span8" name="subtitle"></textarea>
                                        <div class="sepH_b">
                                            <p style="color:#F00; font-size:15px;">{{Session::get('message')}}</p>
                                            <input type="submit" class="btn" value="Publish">
                                        </div>
                                        
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Post Gallery</h4>
                                <div class="btn-group">
                                    <a href="#" class="btn btn-inverse btn-mini delete_rows_dt" data-tableid="dt_gal" title="Edit">Delete</a> 
                                </div>
                            </div>
                            <div class="w-box-content">
                                <table class="table table-vam table-striped" id="smpl_tbl">
                                    <thead>
                                        <tr>
                                            <th class="table_checkbox" style="width:13px"><input type="checkbox" name="select_rows" class="select_rows" data-tableid="dt_gal" /></th>
                                            <th>Image</th>
                                            <th>Title</th>
                                            <th>Sub Title</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($gallery as $_gallery)
                                        <tr>
                                            <td><input type="checkbox" name="row_sel" class="row_sel" /></td>
                                            <td style="width:60px">
                                                <a href="" title="{{$_gallery->GalleryName}}" class="thumbnail">
                                                    <img src="{{asset($_gallery->GalleryImageThumb)}}" style="height:50px;width:50px">
                                                </a>
                                            </td>
                                            <td>{{$_gallery->GalleryName}}</td>
                                            <td>{{{substr($_gallery->GalleryDesc,0,25).'..'}}}</td>
                                            <td>{{date("d/M/Y", strtotime($_gallery->created_at))}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{asset('adm/gallery/'.$_gallery->GalleryId.'/edit')}}" class="btn btn-mini" title="Edit"><i class="icon-pencil"></i></a>
                                                    <a href="{{asset('gallery')}}" class="btn btn-mini" title="View"><i class="icon-eye-open"></i></a>
                                                    <a href="{{asset('adm/gallery/'.$_gallery->GalleryId.'/delete')}}" class="btn btn-mini" title="Delete"><i class="icon-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="w-box-footer">
                                {{$gallery->links('admin.template.custom-paginate')}}
                            </div>
                        </div>
                        <!-- confirmation box -->
                        <div class="hide">
                            <div id="confirm_dialog" class="cbox_content">
                                <div class="sepH_c"><strong>Are you sure you want to delete this row(s)?</strong></div>
                                <div>
                                    <a href="#" class="btn btn-small btn-beoro-3 confirm_yes">Yes</a>
                                    <a href="#" class="btn btn-small confirm_no">No</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
    @include('admin.template.js')
          


    </body>
</html>