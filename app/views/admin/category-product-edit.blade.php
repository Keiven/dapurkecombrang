<!DOCTYPE HTML>
<html lang="en-US">
    <head>
    @include('admin.template.head')
    </head>
    <body class="bg_d">
    @include('admin.template.navbar')
            
        <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box" id="n_fileupload">
                            <div class="w-box-header">
                                <h4>Add Menu Category</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="row-fluid">
                                <form method="post">
                                    <div class="span6">
                                                <div class="input-append">
                                                    <input name="category" value="{{$category->CategoryName}}" type="text" placeholder="Add product category.." size="16" class="span8">
												</div> 

                                        <div class="sepH_b">
                                            <p style="color:#F00; font-size:15px;">{{Session::get('message')}}</p>
                                            <input type="submit" class="btn" value="Publish">
                                        </div>
                                        
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

               
            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
        @include('admin.template.js')


    </body>
</html>