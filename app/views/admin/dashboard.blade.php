<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        @include('admin.template.head')
    </head>
    <body class="bg_d">
        @include('admin.template.navbar')
        <!-- breadcrumbs -->
            <!-- <div class="container">
                <ul id="breadcrumbs">
                    <li><a href="javascript:void(0)"><i class="icon-home"></i></a></li>
                    <li><a href="javascript:void(0)">Content</a></li>
                    <li><a href="javascript:void(0)">Article: Lorem ipsum dolor...</a></li>
                    <li><a href="javascript:void(0)">Comments</a></li>
                    <li><span>Lorem ipsum dolor sit amet...</span></li>
                </ul>
            </div> -->
            
        <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Analytics</h4>
                                <i class="icsw16-graph icsw16-white pull-right"></i>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="slidewrap">
                                    <ul class="slider" id="sliderName">
                                        <li class="slide">  
                                            <span class="hide headName">Pageviews</span>
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div id="ch_pages" class="chart_a"></div>
                                                </div>
                                            </div>
                                            <div class="row-fluid">
                                                <div class="span10 offset1">
                                                
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <div class="anlt_box">
                                                            @if($today_percentage > 0)
                                                                <p class="anlt_heading">Last 24h<span class="up">{{'+'.$today_percentage.'%'}}</span></p>
                                                            @else
                                                                <p class="anlt_heading">Last 24h<span class="down">{{$today_percentage.'%'}}</span></p>
                                                            @endif
                                                                
                                                                <p class="anlt_content">{{$data_today}}</p>
                                                            </div>
                                                        </div>
                                                        <div class="span4">
                                                            <div class="anlt_box">
                                                            @if($weekly_percentage > 0)
                                                                <p class="anlt_heading">Last 7d<span class="up">{{'+'.$weekly_percentage.'%'}}</span></p>
                                                            @else
                                                                <p class="anlt_heading">Last 7d<span class="down">{{$weekly_percentage.'%'}}</span></p>
                                                            @endif
                                                                <p class="anlt_content">{{$data_weekly}}</p>
                                                            </div>
                                                        </div>
                                                        <div class="span4">
                                                            <div class="anlt_box">
                                                            @if($monthly_percentage > 0)
                                                                <p class="anlt_heading">Last month<span class="up">{{'+'.$monthly_percentage.'%'}}</span></p>
                                                            @else
                                                                <p class="anlt_heading">Last month<span class="down">{{$monthly_percentage.'%'}}</span></p>
                                                            @endif
                                                                <p class="anlt_content">{{$data_monthly}}</p>
                                                            </div>
                                                        </div>  
                                                    </div>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <!--<div class="btn-group">
                                    <a href="#" class="btn btn-inverse btn-mini delete_rows_simple" data-tableid="smpl_tbl" title="Edit">Delete</a>
                                </div>-->
                            </div>
                            <div class="w-box-content">
                                <table class="table table-vam table-striped" id="smpl_tbl">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Image</th>
                                            <th>User</th>
                                            <th>Status</th>
                                            <th>Last Seen</th>
                                            <th>Last Login IP</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($userlist as $_userlist)
                                        <tr>
                                            <td>{{$_userlist['UserId']}}</td>
                                            <td style="width:20px;">
                                                <a href="" title="{{$_userlist['FullName']}}" class="thumbnail">
                                                    @if($_userlist['UserPhoto'] != '')
                                                    <img src="{{asset($_userlist['UserPhoto'])}}" style="height:20px;width:20px">
                                                    @else
                                                    <img src="{{asset('asset2/img/dummy_60x60.gif')}}" style="height:20px;width:20px">
                                                    @endif
                                                </a>
                                            </td>
                                            <td>{{$_userlist['Username']}}</td>
                                            <td>@if($_userlist['UserLevel'] > 1) {{'Admin'}} @else {{'Super Admin'}} @endif</td>
                                            @if($_userlist['LastLogin'] != '0000-00-00')
                                            <td>{{$_userlist['LastLoginTime'].' '.date("d-M-Y", strtotime($_userlist['LastLogin']))}}</td>
                                            @else
                                            <td>Never Login</td>
                                            @endif
                                            @if($_userlist['LastLoginIp'] != '')
                                            <td>{{$_userlist['LastLoginIp']}}</td>
                                            @else
                                            <td>Never Login</td>
                                            @endif
                                            <td> 
                                                <div class="btn-group">
                                                    <a href="{{asset('adm/user/profile/'.$_userlist['UserId'])}}" class="btn btn-mini" title="Edit"><i class="icon-pencil"></i></a>
                                                    <a href="{{asset('adm/user/profile/'.$_userlist['UserId'].'/delete')}}" class="btn btn-mini" title="Delete"><i class="icon-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="w-box-footer">
                                {{$userlist->links('admin.template.custom-paginate')}}
                            </div>
                        </div>
                     </div>
                  </div>
            <div class="footer_space"></div>
        </div> 
        
    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
    <!-- Common JS -->
        <!-- jQuery framework -->
            <script src="{{asset('asset2/js/jquery.min.js')}}"></script>
        <!-- bootstrap Framework plugins -->
            <script src="{{asset('asset2/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- top menu -->
            <script src="{{asset('asset2/js/jquery.fademenu.js')}}"></script>
        <!-- top mobile menu -->
            <script src="{{asset('asset2/js/selectnav.min.js')}}"></script>
        <!-- actual width/height of hidden DOM elements -->
            <script src="{{asset('asset2/js/jquery.actual.min.js')}}"></script>
        <!-- jquery easing animations -->
            <script src="{{asset('asset2/js/jquery.easing.1.3.min.js')}}"></script>
        <!-- power tooltips -->
            <script src="{{asset('asset2/js/lib/powertip/jquery.powertip-1.1.0.min.js')}}"></script>
        <!-- date library -->
            <script src="{{asset('asset2/js/moment.min.js')}}"></script>
        <!-- common functions -->
            <script src="{{asset('asset2/js/beoro_common.js')}}"></script>

    <!-- Dashboard JS -->
        <!-- jQuery UI -->
            <script src="{{asset('asset2/js/lib/jquery-ui/jquery-ui-1.9.2.custom.min.js')}}"></script>
        <!-- touch event support for jQuery UI -->
            <script src="{{asset('asset2/js/lib/jquery-ui/jquery.ui.touch-punch.min.js')}}"></script>
        <!-- colorbox -->
            <script src="{{asset('asset2/js/lib/colorbox/jquery.colorbox.min.js')}}"></script>
        <!-- fullcalendar -->
            <!--<script src="{{asset('asset2/s/lib/fullcalendar/fullcalendar.min.js')}}"></script>-->
        <!-- flot charts -->
            <script src="{{asset('asset2/js/lib/flot-charts/jquery.flot.js')}}"></script>
            <script src="{{asset('asset2/js/lib/flot-charts/jquery.flot.resize.js')}}"></script>
            <script src="{{asset('asset2/js/lib/flot-charts/jquery.flot.pie.js')}}"></script>
            <script src="{{asset('asset2/js/lib/flot-charts/jquery.flot.orderBars.js')}}"></script>
            <script src="{{asset('asset2/js/lib/flot-charts/jquery.flot.tooltip.js')}}"></script>
            <script src="{{asset('asset2/js/lib/flot-charts/jquery.flot.time.js')}}"></script>
        <!-- responsive carousel -->
            <script src="{{asset('asset2/js/lib/carousel/plugin.min.js')}}"></script>
        <!-- responsive image grid -->
            <script src="{{asset('asset2/js/lib/wookmark/jquery.imagesloaded.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/wookmark/jquery.wookmark.min.js')}}"></script>

            <script src="{{asset('asset2/js/pages/beoro_dashboard.js')}}"></script>
            

    </body>
</html>