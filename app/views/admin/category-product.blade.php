<!DOCTYPE HTML>
<html lang="en-US">
    <head>
    @include('admin.template.head')
    </head>
    <body class="bg_d">
    @include('admin.template.navbar')
            
        <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box" id="n_fileupload">
                            <div class="w-box-header">
                                <h4>Add Menu Category</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="row-fluid">
                                <form method="post">
                                    <div class="span6">
                                                <div class="input-append">
                                                    <input name="category" type="text" placeholder="Add product category.." size="16" class="span8">
												</div> 

                                        <div class="sepH_b">
                                            <p style="color:#F00; font-size:15px;">{{Session::get('message')}}</p>
                                            <input type="submit" class="btn" value="Publish">
                                        </div>
                                        
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Edit/Remove Menu Category</h4>
                                <div class="btn-group">
                                    <a href="#" class="btn btn-inverse btn-mini delete_rows_dt" data-tableid="dt_gal" title="Edit">Delete</a>
                                </div>
                            </div>
                            <div class="w-box-content">
                                <table class="table table-vam table-striped" id="dt_gal">
                                    <thead>
                                        <tr>
                                            <th class="table_checkbox" style="width:13px"><input type="checkbox" name="select_rows" class="select_rows" data-tableid="dt_gal" /></th>
                                            <th>Category</th>
                                            <th>Total Menu</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0?>
                                    @foreach($category as $_category)
                                        <tr>
                                            <td><input type="checkbox" name="row_sel" class="row_sel" /></td>
                                            <td>{{$_category->CategoryName}}</td>
                                            <td>{{$totalproduct[$i++]}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{asset('adm/menu/category/edit='.Crypt::encrypt($_category->CategoryName))}}" class="btn btn-mini" title="Edit"><i class="icon-pencil"></i></a>
                                                    <a href="{{asset('adm/menu/category/delete='.Crypt::encrypt($_category->CategoryName))}}" class="btn btn-mini" title="Delete"><i class="icon-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- confirmation box -->
                        <div class="hide">
                            <div id="confirm_dialog" class="cbox_content">
                                <div class="sepH_c"><strong>Are you sure you want to delete this row(s)?</strong></div>
                                <div>
                                    <a href="#" class="btn btn-small btn-beoro-3 confirm_yes">Yes</a>
                                    <a href="#" class="btn btn-small confirm_no">No</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
        @include('admin.template.js')


    </body>
</html>