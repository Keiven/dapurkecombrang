<!DOCTYPE HTML>
<html lang="en-US">
    <head>
    	@include('admin.template.head')
    </head>
    <body class="bg_d">
    	@include('admin.template.navbar')

        <!-- breadcrumbs
            <div class="container">
                <ul id="breadcrumbs">
                    <li><a href="javascript:void(0)"><i class="icon-home"></i></a></li>
                    <li><a href="javascript:void(0)">Content</a></li>
                    <li><a href="javascript:void(0)">Article: Lorem ipsum dolor...</a></li>
                    <li><a href="javascript:void(0)">Comments</a></li>
                    <li><span>Lorem ipsum dolor sit amet...</span></li>
                </ul>
            </div> -->
            
        <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                    <div class="span6">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>User profile</h4>
                            </div>
                            <div class="w-box-content cnt_a user_profile">
                                <div class="row-fluid">
                                    <div class="span2">
                                        <div class="img-holder">
                                        @if($user->UserPhoto == "")
                                            <img class="img-avatar" alt="" src="{{asset('asset2/img/avatars/avatar.png')}}">
                                        @else
                                        	<img class="img-avatar" alt="" src="{{asset($user->UserPhoto)}}">
                                        @endif
                                        </div>
                                    </div>
                                    <div class="span10">
                                        <p class="formSep"><small class="muted">Verified:</small> <span class="label label-success">Yes</span></p>
                                        <p class="formSep"><small class="muted">Username:</small> {{$user->Username}}</p>
                                        <p class="formSep"><small class="muted">Name:</small> {{$user->FullName}}</p>
                                        <p class="formSep"><small class="muted">Gender:</small> {{$user->Gender}}</p>
                                        <p class="formSep"><small class="muted">Email:</small> {{{$user->Email}}}</p>
                                        <p class="formSep"><small class="muted">Signature:</small> {{{$user->Signature}}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>User settings</h4>
                            </div>
                            <div class="w-box-content">
                                <form action="{{asset('adm/user/profile/'.$user->UserId.'/save')}}" method="post" enctype="multipart/form-data">
                                    <div class="formSep">
                                        <label>Avatar</label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 60px; height: 60px;">
                                            @if($user->UserPhoto == "")
                                            	<img src="{{asset('asset2/img/dummy_60x60.gif')}}" alt="" >
                                            @else
                                            	<img src="{{asset($user->UserPhoto)}}" alt="" >
                                           	@endif
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="width: 60px; height: 60px;"></div>
                                            <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" name="photo"></span>
                                            <a href="#" class="btn btn-small btn-link fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                    <div class="formSep">
                                        <label for="u_username">Username</label>
                                        <input type="text" id="u_username" name="username" class="span8" value="{{$user->Username}}" />
                                    </div>
                                    <div class="formSep">
                                        <label for="u_name">Name</label>
                                        <input type="text" id="u_name" name="fullname" class="span8" value="{{$user->FullName}}" />
                                    </div>
                                    <div class="formSep">
                                        <label for="u_password">Password</label>
                                        <input type="password" id="u_password" name="password" class="span8" />
                                        <span class="help-block">Enter Password</span>
                                        <input type="password" id="u_repassword" name="repassword" class="span8" />
                                        <span class="help-block">Repeat Password</span>
                                    </div>
                                    <div class="formSep">
                                        <label for="u_email">Email</label>
                                        <input type="text" id="u_email" name="email" class="span8" value="{{$user->Email}}" />
                                    </div>
                                    <div class="formSep">
                                        <label>Gender</label>
                                        <label for="u_male" class="radio inline"><input type="radio" name="gender" id="u_male" @if($user->Gender=='Male')checked="checked"@endif value="Male" /> Male</label>
                                        <label for="u_female" class="radio inline"><input type="radio" name="gender" id="u_female" @if($user->Gender=='Female')checked="checked"@endif value="Female"/> Female</label>
                                    </div>
                                    @if(Auth::user()->UserLevel == 1)
                                    <div class="formSep">
                                        <label>Admin Level</label>
                                        <label for="gender" class="radio inline"><input type="radio" name="userlevel" @if($user->UserLevel == 1)checked="checked"@endif value="1"id="u_male"/>Superadmin</label>
                                        <label for="gender" class="radio inline"><input type="radio" name="userlevel" @if($user->UserLevel > 1)checked="checked"@endif value="2"id="u_female"/>Admin</label>
                                    </div>
                                    @endif
                                    <div class="formSep">
                                        <label for="u_signature">Signature</label>
                                        <textarea name="signature" id="u_signature" cols="30" rows="4" class="span8">{{$user->Signature}}</textarea>
                                    </div>
                                    <div class="formSep sepH_b">
                                    	<p style="color:#F00; font-size:15px;">{{Session::get('message')}}</p>
                                        <button class="btn btn-beoro-3" type="submit">Save changes</button>
                                        <a href="#" class="btn btn-link">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
    <!-- Common JS -->
        <!-- jQuery framework -->
            <script src="{{asset('asset2/js/jquery.min.js')}}"></script>
        <!-- bootstrap Framework plugins -->
            <script src="{{asset('asset2/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- top menu -->
            <script src="{{asset('asset2/js/jquery.fademenu.js')}}"></script>
        <!-- top mobile menu -->
            <script src="{{asset('asset2/js/selectnav.min.js')}}"></script>
        <!-- actual width/height of hidden DOM elements -->
            <script src="{{asset('asset2/js/jquery.actual.min.js')}}"></script>
        <!-- jquery easing animations -->
            <script src="{{asset('asset2/js/jquery.easing.1.3.min.js')}}"></script>
        <!-- power tooltips -->
            <script src="{{asset('asset2/js/lib/powertip/jquery.powertip-1.1.0.min.js')}}"></script>
        <!-- date library -->
            <script src="{{asset('asset2/js/moment.min.js')}}"></script>
        <!-- common functions -->
            <script src="{{asset('asset2/js/beoro_common.js')}}"></script>

        <!-- file upload widget -->
            <script src="{{asset('asset2/js/form/bootstrap-fileupload.min.js')}}"></script>

    </body>
</html>