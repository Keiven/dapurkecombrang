<!-- Common JS -->
        <!-- jQuery framework -->
            <script src="{{asset('asset2/js/jquery.min.js')}}"></script>
        <!-- bootstrap Framework plugins -->
            <script src="{{asset('asset2/js/bootstrap.min.js')}}"></script>
        <!-- top menu -->
            <script src="{{asset('asset2/js/jquery.fademenu.js')}}"></script>
        <!-- top mobile menu -->
            <script src="{{asset('asset2/js/selectnav.min.js')}}"></script>
        <!-- actual width/height of hidden DOM elements -->
            <script src="{{asset('asset2/js/jquery.actual.min.js')}}"></script>
        <!-- jquery easing animations -->
            <script src="{{asset('asset2/js/jquery.easing.1.3.min.js')}}"></script>
        <!-- power tooltips -->
            <script src="{{asset('asset2/js/lib/powertip/jquery.powertip-1.1.0.min.js')}}"></script>
        <!-- date library -->
            <script src="{{asset('asset2/js/moment.min.js')}}"></script>
        <!-- common functions -->
            <script src="{{asset('asset2/js/beoro_common.js')}}"></script>

    <!-- Forms -->  
        <!-- jQuery UI -->
            <script src="{{asset('asset2/js/lib/jquery-ui/jquery-ui-1.9.2.custom.min.js')}}"></script>
        <!-- touch event support for jQuery UI -->
            <script src="{{asset('asset2/js/lib/jquery-ui/jquery.ui.touch-punch.min.js')}}"></script>
        <!-- progressbar animations -->
            <script src="{{asset('asset2/js/form/jquery.progressbar.anim.min.js')}}"></script>
        <!-- 2col multiselect -->
            <script src="{{asset('asset2/js/lib/multi-select/js/jquery.multi-select.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/multi-select/js/jquery.quicksearch.min.js')}}"></script>
        <!-- combobox -->
            <script src="{{asset('asset2/js/form/fuelux.combobox.min.js')}}"></script>
        <!-- file upload widget -->
            <script src="{{asset('asset2/js/form/bootstrap-fileupload.min.js')}}"></script>
        <!-- masked inputs -->
            <script src="{{asset('asset2/js/lib/jquery-inputmask/jquery.inputmask.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jquery-inputmask/jquery.inputmask.extensions.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jquery-inputmask/jquery.inputmask.date.extensions.js')}}"></script>
        <!-- enchanced select box, tag handler -->
            <script src="{{asset('asset2/js/lib/select2/select2.min.js')}}"></script>
        <!-- password strength metter -->
            <script src="{{asset('asset2/js/lib/pwdMeter/jquery.pwdMeter.min.js')}}"></script>
        <!-- datepicker -->
            <script src="{{asset('asset2/js/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <!-- timepicker -->
            <script src="{{asset('asset2/js/lib/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
        <!-- colorpicker -->
            <script src="{{asset('asset2/js/lib/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
        <!-- metadata -->
            <script src="{{asset('asset2/js/lib/ibutton/js/jquery.metadata.min.js')}}"></script>
        <!-- switch buttons -->
            <script src="{{asset('asset2/js/lib/ibutton/js/jquery.ibutton.beoro.min.js')}}"></script>
        <!-- autosize textarea -->
            <script src="{{asset('asset2/js/form/jquery.autosize.min.js')}}"></script>
        <!-- textarea counter -->
            <script src="{{asset('asset2/js/lib/jquery-textarea-counter/jquery.textareaCounter.plugin.min.js')}}"></script>
        <!-- UI Spinners -->
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/globalize.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.fr-FR.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.ja-JP.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.zh-CN.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/compiled/jqamp-ui-spinner.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/compiled/jquery-mousewheel-3.0.6.min.js')}}"></script>
        <!-- plupload and the jQuery queue widget -->
            <script type="text/javascript" src="{{asset('asset2/js/lib/plupload/js/plupload.full.js')}}"></script>
            <script type="text/javascript" src="{{asset('asset2/js/lib/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js')}}"></script>
        <!-- WYSIWG Editor is here-->
            <script src="{{asset('asset2/js/lib/ckeditor/ckeditor.js')}}"></script>

            <script src="{{asset('asset2/js/pages/beoro_form_elements.js')}}"></script>
        <!-- datatables -->
            <script src="{{asset('asset2/js/lib/datatables/js/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/datatables/js/jquery.dataTables.sorting.js')}}"></script>
        <!-- datatables bootstrap integration -->
            <script src="{{asset('asset2/js/lib/datatables/js/jquery.dataTables.bootstrap.min.js')}}"></script>
        <!-- colorbox -->
            <script src="{{asset('asset2/js/lib/colorbox/jquery.colorbox.min.js')}}"></script>
        <!-- responsive image grid -->
            <script src="{{asset('asset2/js/lib/wookmark/jquery.imagesloaded.min.js')}}"></script>
                    <!-- responsive image grid -->
            <script src="{{asset('asset2/js/lib/wookmark/jquery.imagesloaded.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/wookmark/jquery.wookmark.min.js')}}"></script>

            <script src="{{asset('asset2/js/pages/beoro_gallery.js')}}"></script>
            <script src="{{asset('asset2/js/pages/beoro_tables.js')}}"></script>