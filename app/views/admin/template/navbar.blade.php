<!-- main wrapper (without footer) -->    
<div class="main-wrapper">
        <!-- top bar -->
            <div class="navbar navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <div id="fade-menu" class="pull-left">
                            <ul class="clearfix" id="mobile-nav">
                                <li><a href="{{asset('adm/dashboard')}}">Dashboard</a></li>
                                <li>
                                    <a href="javascript:void(0)">Static Page</a>
                                    <ul>
                                        <li>
                                            <a href="{{asset('adm/opening-page')}}">Opening Page</a>
                                            <a href="{{asset('adm/about')}}">About</a>
                                            <a href="{{asset('adm/banner')}}">Banner</a>
                                            <a href="{{asset('adm/services')}}">Services</a>
                                        </li>
                                        <!-- <li>
                                            <a href="#">About</a>
                                        <ul>
                                            <li>
                                                <a href="form_validation.html">About Company</a>
                                            </li>
                                            <li>
                                                <a href="form_validation.html">About Owner</a>
                                            </li>
                                        </li>
                                        </ul> -->
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0)">Menu Page</a>
                                    <ul>
                                        <li>
                                            <a href="{{asset('adm/menu')}}">Menu Detail</a>
                                            <a href="{{asset('adm/menu/category/add')}}">Menu Category Detail</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0)">Gallery Page</a>
                                    <ul>
                                        <li><a href="{{asset('adm/gallery')}}">Gallery Detail</a></li>
                                    </ul>
                                </li>
                                @if(false)
                                <li><a href="javascript:void(0)">News Page</a>
                                    <ul>
                                        <li><a href="{{asset('adm/news')}}">News Detail</a></li>
                                    </ul>
                                </li>
                                 <li><a href="javascript:void(0)">Media Page</a>
                                    <ul>
                                        <li><a href="{{asset('adm/media')}}">Media</a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0)">Images</a>
                                    <ul>
                                        <li><a href="{{asset('adm/slider-image')}}">Slider Images</a></li>
                                        
                                    </ul>
                                </li>
                                @endif
                                <li><a href="{{asset('adm/user/management')}}">User Management</a></li>
                                <li><a href="{{asset('adm/setting')}}">Site Setting</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        <!-- header -->
            <header>
                <div class="container">
                    <div class="row">
                        <div class="span3">
                            <div class="main-logo"><a href="dashboard.html"><img src="{{asset('asset2/img/beoro_logo.png')}}" alt="Beoro Admin"></a></div>
                        </div>
                        <div class="span5">
                        </div>
                        <div class="span4">
                            <div class="user-box">
                                <div class="user-box-inner">
                                    @if(Auth::user()->UserPhoto == '')
                                    <img src="{{asset('asset2/img/avatars/avatar.png')}}" alt="" class="user-avatar img-avatar">
                                    @else
                                    <img src="{{asset(Auth::user()->UserPhoto)}}" alt="" class="user-avatar img-avatar">
                                    @endif
                                    <div class="user-info">
                                        Welcome, <strong>{{Auth::user()->Username}}</strong>
                                        <ul class="unstyled">
                                            <li><a href="{{asset('adm/user/profile/'.Auth::user()->UserId)}}">Settings</a></li>
                                            <li>&middot;</li>
                                            <li><a href="{{asset('adm/logout')}}">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>