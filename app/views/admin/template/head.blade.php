<meta charset="UTF-8">
<title>DapurKecombrang Admin Dashboard</title>
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<link rel="icon" type="image/ico" href="favicon.ico">

<!-- common stylesheets-->
<!-- bootstrap framework css -->
<link rel="stylesheet" href="{{asset('asset2/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('asset2/css/bootstrap-responsive.min.css')}}">
<!-- iconSweet2 icon pack (16x16) -->
<link rel="stylesheet" href="{{asset('asset2/img/icsw2_16/icsw2_16.css')}}">
<!-- splashy icon pack -->
<link rel="stylesheet" href="{{asset('asset2/img/splashy/splashy.css')}}">
<!-- flag icons -->
<link rel="stylesheet" href="{{asset('asset2/img/flags/flags.css')}}">
<!-- power tooltips -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/powertip/jquery.powertip.css')}}">
<!-- google web fonts -->
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Abel">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300">

<!-- aditional stylesheets -->
<!-- jQuery UI theme -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/jquery-ui/css/Aristo/Aristo.css')}}">
<!-- 2 col multiselect -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/multi-select/css/multi-select.css')}}">
<!-- enchanced select box, tag handler -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/select2/select2.css')}}">
<!-- datepicker -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/bootstrap-datepicker/css/datepicker.css')}}">
<!-- timepicker -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/bootstrap-timepicker/css/timepicker.css')}}">
<!-- colorpicker -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/bootstrap-colorpicker/css/colorpicker.css')}}">
<!-- switch buttons -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/ibutton/css/jquery.ibutton.css')}}">
<!-- UI Spinners -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/jqamp-ui-spinner/css/jqamp-ui-spinner.css')}}">
<!-- multiupload -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/plupload/js/jquery.plupload.queue/css/plupload-beoro.css')}}">
<!-- colorbox -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/colorbox/colorbox.css')}}">
<!-- datatables -->
<link rel="stylesheet" href="{{asset('asset2/js/lib/datatables/css/datatables_beoro.css')}}">


<!-- main stylesheet -->
<link rel="stylesheet" href="{{asset('asset2/css/beoro.css')}}">
<style>textarea{ height: 100px;}</style>
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css"><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="css/ie9.css"><![endif]-->
    
<!--[if lt IE 9]>
    <script src="js/ie/html5shiv.min.js"></script>
    <script src="js/ie/respond.min.js"></script>
    <script src="js/lib/flot-charts/excanvas.min.js"></script>
<![endif]-->