<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        @include('admin.template.head')
    </head>
    <body class="bg_d">
        @include('admin.template.navbar')
            
        <!-- main content -->
            <div class="container">
                
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Head Banner</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="row-fluid">
                                    <div class="span6">
                                    	<form method="post" enctype="multipart/form-data" action="/adm/banner/head">
                                    	<div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;">
                                            @if($desc->DescBannerImage == '')
                                                <img src="{{asset('asset2/img/dummy_150x150.gif')}}" alt="">
                                            @else
                                                <img src="{{asset($desc->DescBannerImage)}}" alt="">
                                            @endif
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-small btn-file"><span class="fileupload-new">Select Image</span><span class="fileupload-exists">Change</span><input type="file" name="photo"></span>
                                                <span class="help-block">min size 400 x 300  px.</span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div><br>

                                        <div class="w-box">
                                            <label>Banner Title</label>
                                        	<input name="title" type="text" placeholder="Type something…" class="span8" value="{{$desc->DescBannerTitle}}">
                                        </div>

                                        <div class="w-box">
                                            <label>Banner Sub Title</label>
                                        	<textarea rows="2" class="span8" name="desc">{{$desc->DescBannerSubtitle}}</textarea>
                                        </div>

                                        <div class="w-box">
                                            <label>Banner Phone Number</label>
                                        	<input name="phone" type="text" placeholder="Type something…" class="span8" value="{{$desc->DescBannerPhone}}">
                                        </div>

                                        <div class="w-box">
                                            <label>Banner Email</label>
                                        	<input name="email" type="text" placeholder="Type something…" class="span8" value="{{$desc->DescBannerEmail}}">
                                        </div><br>

                                        <label>Banner Welcome</label>
                                        <div class="w-box-content cnt_no_pad">
                                            <textarea name="welcome" id="wysiwg_editor2" cols="30" rows="10">{{$desc->DescWelcome}}</textarea>
                                        </div><br>

                                        <div class="sepH_b">
                                            <p style="color:#F00; font-size:15px;">{{Session::get('head')}}</p>
                                            <button type="submit" class="btn">Publish</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <br>

                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Body Banner</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="row-fluid">
                                    <div class="span6">
                                    	<form method="post" enctype="multipart/form-data" action="/adm/banner/body">
                                    	<div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;">
                                            @if($desc->DescBodyBannerImage == '')
                                                <img src="{{asset('asset2/img/dummy_150x150.gif')}}" alt="">
                                            @else
                                                <img src="{{asset($desc->DescBodyBannerImage)}}" alt="">
                                            @endif
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-small btn-file"><span class="fileupload-new">Select Image</span><span class="fileupload-exists">Change</span><input type="file" name="photo"></span>
                                                <span class="help-block">min size 400 x 300  px.</span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div><br>


                                        <div class="w-box">
                                            <label>Banner Sub Title</label>
                                        	<textarea rows="2" class="span8" name="desc">{{$desc->DescBodyBannerTitle}}</textarea>
                                        </div>

                                        <div class="sepH_b">
                                            <p style="color:#F00; font-size:15px;">{{Session::get('body')}}</p>
                                            <button type="submit" class="btn">Publish</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Footer Banner</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <form method="post" action="/adm/banner/footer">
                                        <label>Footer Address</label>
                                        <div class="w-box-content cnt_no_pad">
                                            <textarea name="content" id="wysiwg_editor" cols="30" rows="10">{{$desc->DescFooterBannerAddress}}</textarea>
                                        </div><br>


                                        <div class="sepH_b">
                                            <p style="color:#F00; font-size:15px;">{{Session::get('footer')}}</p>
                                            <button type="submit" class="btn">Publish</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                

            </div>

            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        @include('admin.template.footer')
        
        @include('admin.template.js')

    </body>
</html>