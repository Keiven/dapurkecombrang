<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        @include('admin.template.head')
    </head>
    <body class="bg_d">
    <!-- main wrapper (without footer) -->    
        @include('admin.template.navbar')

        <!-- breadcrumbs -->
            <!-- <div class="container">
                <ul id="breadcrumbs">
                    <li><a href="javascript:void(0)"><i class="icon-home"></i></a></li>
                    <li><a href="javascript:void(0)">Content</a></li>
                    <li><a href="javascript:void(0)">Article: Lorem ipsum dolor...</a></li>
                    <li><a href="javascript:void(0)">Comments</a></li>
                    <li><span>Lorem ipsum dolor sit amet...</span></li>
                </ul>
            </div> -->
            
        <!-- main content -->
            <div class="container">

                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box" id="n_fileupload">
                            <div class="w-box-header">
                                <h4>Menu Text Description</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <form method="post" action="/adm/menu/banner/edit">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <label>Description</label>
                                        <textarea rows="2" class="span8" name="desc">{{$desc->DescProduct}}</textarea>
                                        <div class="sepH_b">
                                            <p style="color:#F00; font-size:15px;">{{Session::get('message2')}}</p>
                                            <input type="submit" class="btn" value="Publish">
                                        </div>
                                        
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box" id="n_fileupload">
                            <div class="w-box-header">
                                <h4>Menu Upload</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <form method="post" enctype="multipart/form-data">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;">
                                                <img src="{{asset('asset2/img/dummy_150x150.gif')}}" alt="">
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" name="photo"></span>
                                                <span class="help-block">Best view 800 x 800px.</span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                            <label>Menu Title</label>
                                            <input name="title" type="text" placeholder="Type something…" class="span8">
                                            <label>Menu Detail</label>
                                            <textarea name="detail" id="wysiwg_editor" cols="30" rows="10"></textarea>
                                            <label>Menu Price</label>
                                            <input name="price" type="text" placeholder="Ex: 20K or 20.000" class="span8">

										<label>Menu Category</label>
                                        <select id="s2_single" class="span8" name="category">
                                            <option value=""></option>
                                            @foreach($category as $_category)
                                            <option value="{{str_replace(' ','-',$_category->CategoryName)}}">{{$_category->CategoryName}}</option>
                                            @endforeach
                                        </select>
                                        <br>      
                                        <div class="sepH_b">
                                            <p style="color:#F00; font-size:15px;">{{Session::get('message')}}</p>
                                            <input type="submit" class="btn" value="Publish">
                                        </div>
                                        
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                

                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Post Product</h4>
                                <div class="btn-group">
                                    <a href="#" class="btn btn-inverse btn-mini delete_rows_dt" data-tableid="dt_gal" title="Edit">Delete</a> 
                                </div>
                            </div>
                            <div class="w-box-content">
                                <table class="table table-vam table-striped" id="dt_gal">
                                    <thead>
                                        <tr>
                                            <th class="table_checkbox" style="width:13px"><input type="checkbox" name="select_rows" class="select_rows" data-tableid="dt_gal" /></th>
                                            <th>Image</th>
                                            <th>Title</th>
                                            <th>Category</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($product as $_product)
                                        <tr>
                                            <td><input type="checkbox" name="row_sel" class="row_sel" /></td>
                                            <td style="width:60px">
                                                <a href="" title="{{$_product->ProductName}}" class="thumbnail">
                                                    <img src="{{asset($_product->ProductImageThumb)}}" style="height:50px;width:50px">
                                                </a>
                                            </td>
                                            <td>{{{$_product->ProductName}}}</td>
                                            <td>{{$_product->CategoryName}}</td>
                                            <td>{{date("d/M/Y", strtotime($_product->created_at))}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{asset('adm/menu/'.$_product->ProductId.'/edit')}}" class="btn btn-mini" title="Edit"><i class="icon-pencil"></i></a>
                                                    <a href="{{asset('product')}}" class="btn btn-mini" title="View"><i class="icon-eye-open"></i></a>
                                                    <a href="{{asset('adm/menu/'.$_product->ProductId.'/delete')}}" class="btn btn-mini" title="Delete"><i class="icon-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>

                            </div>
                            <div class="w-box-footer">
                                {{$product->links('admin.template.custom-paginate')}}
                            </div>
                        </div>
                        <!-- confirmation box -->
                        <div class="hide">
                            <div id="confirm_dialog" class="cbox_content">
                                <div class="sepH_c"><strong>Are you sure you want to delete this row(s)?</strong></div>
                                <div>
                                    <a href="#" class="btn btn-small btn-beoro-3 confirm_yes">Yes</a>
                                    <a href="#" class="btn btn-small confirm_no">No</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Dapur Kecombrang 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
    <!-- Common JS -->
        <!-- jQuery framework -->
            <script src="{{asset('asset2/js/jquery.min.js')}}"></script>
        <!-- bootstrap Framework plugins -->
            <script src="{{asset('asset2/js/bootstrap.min.js')}}"></script>
        <!-- top menu -->
            <script src="{{asset('asset2/js/jquery.fademenu.js')}}"></script>
        <!-- top mobile menu -->
            <script src="{{asset('asset2/js/selectnav.min.js')}}"></script>
        <!-- actual width/height of hidden DOM elements -->
            <script src="{{asset('asset2/js/jquery.actual.min.js')}}"></script>
        <!-- jquery easing animations -->
            <script src="{{asset('asset2/js/jquery.easing.1.3.min.js')}}"></script>
        <!-- power tooltips -->
            <script src="{{asset('asset2/js/lib/powertip/jquery.powertip-1.1.0.min.js')}}"></script>
        <!-- date library -->
            <script src="{{asset('asset2/js/moment.min.js')}}"></script>
        <!-- common functions -->
            <script src="{{asset('asset2/js/beoro_common.js')}}"></script>

    <!-- Forms -->  
        <!-- jQuery UI -->
            <script src="{{asset('asset2/js/lib/jquery-ui/jquery-ui-1.9.2.custom.min.js')}}"></script>
        <!-- touch event support for jQuery UI -->
            <script src="{{asset('asset2/js/lib/jquery-ui/jquery.ui.touch-punch.min.js')}}"></script>
        <!-- progressbar animations -->
            <script src="{{asset('asset2/js/form/jquery.progressbar.anim.min.js')}}"></script>
        <!-- 2col multiselect -->
            <script src="{{asset('asset2/js/lib/multi-select/js/jquery.multi-select.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/multi-select/js/jquery.quicksearch.min.js')}}"></script>
        <!-- combobox -->
            <script src="{{asset('asset2/js/form/fuelux.combobox.min.js')}}"></script>
        <!-- file upload widget -->
            <script src="{{asset('asset2/js/form/bootstrap-fileupload.min.js')}}"></script>
        <!-- masked inputs -->
            <script src="{{asset('asset2/js/lib/jquery-inputmask/jquery.inputmask.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jquery-inputmask/jquery.inputmask.extensions.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jquery-inputmask/jquery.inputmask.date.extensions.js')}}"></script>
        <!-- enchanced select box, tag handler -->
            <script src="{{asset('asset2/js/lib/select2/select2.min.js')}}"></script>
        <!-- password strength metter -->
            <script src="{{asset('asset2/js/lib/pwdMeter/jquery.pwdMeter.min.js')}}"></script>
        <!-- datepicker -->
            <script src="{{asset('asset2/js/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <!-- timepicker -->
            <script src="{{asset('asset2/js/lib/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
        <!-- colorpicker -->
            <script src="{{asset('asset2/js/lib/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
        <!-- metadata -->
            <script src="{{asset('asset2/js/lib/ibutton/js/jquery.metadata.min.js')}}"></script>
        <!-- switch buttons -->
            <script src="{{asset('asset2/js/lib/ibutton/js/jquery.ibutton.beoro.min.js')}}"></script>
        <!-- autosize textarea -->
            <script src="{{asset('asset2/js/form/jquery.autosize.min.js')}}"></script>
        <!-- textarea counter -->
            <script src="{{asset('asset2/js/lib/jquery-textarea-counter/jquery.textareaCounter.plugin.min.js')}}"></script>
        <!-- UI Spinners -->
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/globalize.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.fr-FR.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.ja-JP.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.zh-CN.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/compiled/jqamp-ui-spinner.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/compiled/jquery-mousewheel-3.0.6.min.js')}}"></script>
        <!-- plupload and the jQuery queue widget -->
            <script type="text/javascript" src="{{asset('asset2/js/lib/plupload/js/plupload.full.js')}}"></script>
            <script type="text/javascript" src="{{asset('asset2/js/lib/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js')}}"></script>
        <!-- WYSIWG Editor -->
            <script src="{{asset('asset2/js/lib/ckeditor/ckeditor.js')}}"></script>

            <script src="{{asset('asset2/js/pages/beoro_form_elements.js')}}"></script>
        <!-- datatables -->
            <script src="{{asset('asset2/js/lib/datatables/js/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/datatables/js/jquery.dataTables.sorting.js')}}"></script>
        <!-- datatables bootstrap integration -->
            <script src="{{asset('asset2/js/lib/datatables/js/jquery.dataTables.bootstrap.min.js')}}"></script>
        <!-- colorbox -->
            <script src="{{asset('asset2/js/lib/colorbox/jquery.colorbox.min.js')}}"></script>
        <!-- responsive image grid -->
            <script src="{{asset('asset2/js/lib/wookmark/jquery.imagesloaded.min.js')}}"></script>
                    <!-- responsive image grid -->
            <script src="{{asset('asset2/js/lib/wookmark/jquery.imagesloaded.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/wookmark/jquery.wookmark.min.js')}}"></script>

            <script src="{{asset('asset2/js/pages/beoro_gallery.js')}}"></script>
            <script src="{{asset('asset2/js/pages/beoro_tables.js')}}"></script>
          

    </body>
</html>