<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        @include('admin.template.head')
    </head>
    <body class="bg_d">
        @include('admin.template.navbar')
        <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box w-box-green">
                            <div class="w-box-header">
                                <h4>Site</h4>
                                <i class="icsw16-settings icsw16-white pull-right"></i>
                            </div>
                            <div class="w-box-content cnt_a">
                            <form method="post">  
                                <div class="row-fluid">
                                    <div class="span6">
                                        <p class="heading_a">Site settings</p>
                                        <div class="formSep">
                                            <label for="s_name">Site Name</label>
                                            <input type="text" class="span8" id="s_name" name="name" value="{{$setting->SettingSiteName}}" />
                                        </div>
                                        <div class="formSep">
                                            <label for="s_offline">Use Opening Page</label>
                                            @if($setting->SettingOpeningPage == 1)
                                            <input type="checkbox" id="s_offline" name="opening" checked/>
                                            @else
                                            <input type="checkbox" id="s_offline" name="opening"/>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <p class="heading_a">SEO settings</p>
                                        <div class="formSep">
                                            <label for="s_seo_engine_form">SEO Meta Tag</label>
                                            <textarea name="meta" id="s_seo_engine_form" cols="30" rows="4" class="span8">{{$setting->SettingMeta}}</textarea>
                                        </div>
                                      </div>
                            <div class="w-box-footer">
                                <div class="f-center">
                                    <button class="btn btn-beoro-3" type="submit">Save</button>
                                    <button class="btn btn-link inv-cancel">Cancel</button>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
    <!-- Common JS -->
        <!-- jQuery framework -->
            <script src="{{asset('asset2/js/jquery.min.js')}}"></script>
        <!-- bootstrap Framework plugins -->
            <script src="{{asset('asset2/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- top menu -->
            <script src="{{asset('asset2/js/jquery.fademenu.js')}}"></script>
        <!-- top mobile menu -->
            <script src="{{asset('asset2/js/selectnav.min.js')}}"></script>
        <!-- actual width/height of hidden DOM elements -->
            <script src="{{asset('asset2/js/jquery.actual.min.js')}}"></script>
        <!-- jquery easing animations -->
            <script src="{{asset('asset2/js/jquery.easing.1.3.min.js')}}"></script>
        <!-- power tooltips -->
            <script src="{{asset('asset2/js/lib/powertip/jquery.powertip-1.1.0.min.js')}}"></script>
        <!-- date library -->
            <script src="{{asset('asset2/js/moment.min.js')}}"></script>
        <!-- common functions -->
            <script src="{{asset('asset2/js/beoro_common.js')}}"></script>

        <!-- switch buttons -->
            <script src="{{asset('asset2/js/lib/ibutton/js/jquery.ibutton.beoro.min.js')}}"></script>
        <!-- enchanced select box, tag handler -->
            <script src="{{asset('asset2/js/lib/select2/select2.min.js')}}"></script>

            <script src="{{asset('asset2/js/pages/beoro_settings.js')}}"></script>
    </body>
</html>