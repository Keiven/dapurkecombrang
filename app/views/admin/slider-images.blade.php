<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        @include('admin.template.head')
    </head>
    <body class="bg_d">
        @include('admin.template.navbar')
    <!-- main wrapper (without footer) -->    
        
        <!-- breadcrumbs -->
            <!-- <div class="container">
                <ul id="breadcrumbs">
                    <li><a href="javascript:void(0)"><i class="icon-home"></i></a></li>
                    <li><a href="javascript:void(0)">Content</a></li>
                    <li><a href="javascript:void(0)">Article: Lorem ipsum dolor...</a></li>
                    <li><a href="javascript:void(0)">Comments</a></li>
                    <li><span>Lorem ipsum dolor sit amet...</span></li>
                </ul>
            </div> -->
            
        <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box" id="n_fileupload">
                            <div class="w-box-header">
                                <h4>Slider Images Upload</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="row-fluid">
                                    <div class="span6">
                                    <form method="post" enctype="multipart/form-data">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 150px; height: 150px;">
                                                <img src="{{asset('asset2/img/dummy_150x150.gif')}}" alt="">
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-small btn-file"><span class="fileupload-new">Select image backround</span><span class="fileupload-exists">Change</span><input type="file" name="photo"></span>
                                                <span class="help-block">Best view 940 x 410px.</span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
										<label>Select Slider</label>
                                        <select id="s2_single" class="span8" name="id">
                                            <option value=""></option>
                                            <option value="1">Slider 1</option>
                                            <option value="2">Slider 2</option>
                                            <option value="3">Slider 3</option>
                                            <option value="4">Slider 4</option>
                                            <option value="5">Slider 5</option>
                                        </select>
                                        <br>
                                        <label>Select Slider Transition</label>
                                        <select id="s2_single" class="span8" name="transition">
                                            <option value="3dcurtain-vertical">3Dcurtain Vertical</option>
                                            <option value="slotzoom-horizontal">Slotzoom Horizontal</option>
                                            <option value="3dcurtain-horizontal">3Dcurtain Horizontal</option>
                                        </select>
                                        <div class="sepH_b">
                                            <p style="color:#F00; font-size:15px;">{{Session::get('message')}}</p>
                                            <button class="btn" type="submit">Publish</button>
                                        </div>
                                    </form>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                

                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Gallery</h4>
                                <div class="pull-right">
                                    <div class="toggle-group">
                                        <span class="dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></span>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" class="edit_selected_img">Edit selected</a></li>
                                            <li><a href="#" class="remove_selected_img">Remove selected</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div id="large_grid" class="wmk_grid">
                                    <ul>
                                    @foreach($slider as $_slider)
                                        @if($_slider->SliderImage == '')<?php continue ?> @endif
                                        <li id="5100767f8e415" class="folder_all slider_1" data-folder="slider_1">
                                            <span class="img_holder">
                                                <img src="{{asset($_slider->SliderImage)}}" alt="" />
                                                <span class="imgTitle">Slider {{$_slider->SliderId}}</span>
                                            </span>
                                            <p class="img_actions">
                                                <a class="img_action_zoom" href="{{asset($_slider->SliderImage)}}" title="Slider 1"><i class="icon-search"></i></a>
                                                <a href="{{asset('adm/slider-image/'.$_slider->SliderId.'/delete')}}" title="Remove"><i class="icon-trash"></i></a>
                                            </p>
                                        </li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>

            </div>

            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
  <!-- Common JS -->
        <!-- jQuery framework -->
            <script src="{{asset('asset2/js/jquery.min.js')}}"></script>
        <!-- bootstrap Framework plugins -->
            <script src="{{asset('asset2/js/bootstrap.min.js')}}"></script>
        <!-- top menu -->
            <script src="{{asset('asset2/js/jquery.fademenu.js')}}"></script>
        <!-- top mobile menu -->
            <script src="{{asset('asset2/js/selectnav.min.js')}}"></script>
        <!-- actual width/height of hidden DOM elements -->
            <script src="{{asset('asset2/js/jquery.actual.min.js')}}"></script>
        <!-- jquery easing animations -->
            <script src="{{asset('asset2/js/jquery.easing.1.3.min.js')}}"></script>
        <!-- power tooltips -->
            <script src="{{asset('asset2/js/lib/powertip/jquery.powertip-1.1.0.min.js')}}"></script>
        <!-- date library -->
            <script src="{{asset('asset2/js/moment.min.js')}}"></script>
        <!-- common functions -->
            <script src="{{asset('asset2/js/beoro_common.js')}}"></script>

    <!-- Forms -->  
        <!-- jQuery UI -->
            <script src="{{asset('asset2/js/lib/jquery-ui/jquery-ui-1.9.2.custom.min.js')}}"></script>
        <!-- touch event support for jQuery UI -->
            <script src="{{asset('asset2/js/lib/jquery-ui/jquery.ui.touch-punch.min.js')}}"></script>
        <!-- progressbar animations -->
            <script src="{{asset('asset2/js/form/jquery.progressbar.anim.min.js')}}"></script>
        <!-- 2col multiselect -->
            <script src="{{asset('asset2/js/lib/multi-select/js/jquery.multi-select.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/multi-select/js/jquery.quicksearch.min.js')}}"></script>
        <!-- combobox -->
            <script src="{{asset('asset2/js/form/fuelux.combobox.min.js')}}"></script>
        <!-- file upload widget -->
            <script src="{{asset('asset2/js/form/bootstrap-fileupload.min.js')}}"></script>
        <!-- masked inputs -->
            <script src="{{asset('asset2/js/lib/jquery-inputmask/jquery.inputmask.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jquery-inputmask/jquery.inputmask.extensions.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jquery-inputmask/jquery.inputmask.date.extensions.js')}}"></script>
        <!-- enchanced select box, tag handler -->
            <script src="{{asset('asset2/js/lib/select2/select2.min.js')}}"></script>
        <!-- password strength metter -->
            <script src="{{asset('asset2/js/lib/pwdMeter/jquery.pwdMeter.min.js')}}"></script>
        <!-- datepicker -->
            <script src="{{asset('asset2/js/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <!-- timepicker -->
            <script src="{{asset('asset2/js/lib/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
        <!-- colorpicker -->
            <script src="{{asset('asset2/js/lib/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
        <!-- metadata -->
            <script src="{{asset('asset2/js/lib/ibutton/js/jquery.metadata.min.js')}}"></script>
        <!-- switch buttons -->
            <script src="{{asset('asset2/js/lib/ibutton/js/jquery.ibutton.beoro.min.js')}}"></script>
        <!-- autosize textarea -->
            <script src="{{asset('asset2/js/form/jquery.autosize.min.js')}}"></script>
        <!-- textarea counter -->
            <script src="{{asset('asset2/js/lib/jquery-textarea-counter/jquery.textareaCounter.plugin.min.js')}}"></script>
        <!-- UI Spinners -->
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/globalize.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.fr-FR.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.ja-JP.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.zh-CN.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/compiled/jqamp-ui-spinner.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/jqamp-ui-spinner/compiled/jquery-mousewheel-3.0.6.min.js')}}"></script>
        <!-- plupload and the jQuery queue widget -->
            <script type="text/javascript" src="{{asset('asset2/js/lib/plupload/js/plupload.full.js')}}"></script>
            <script type="text/javascript" src="{{asset('asset2/js/lib/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js')}}"></script>
        <!-- WYSIWG Editor -->
            <script src="{{asset('asset2/js/lib/ckeditor/ckeditor.js')}}"></script>

            <script src="{{asset('asset2/js/pages/beoro_form_elements.js')}}"></script>
        <!-- datatables -->
            <script src="{{asset('asset2/js/lib/datatables/js/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/datatables/js/jquery.dataTables.sorting.js')}}"></script>
        <!-- datatables bootstrap integration -->
            <script src="{{asset('asset2/js/lib/datatables/js/jquery.dataTables.bootstrap.min.js')}}"></script>
        <!-- colorbox -->
            <script src="{{asset('asset2/js/lib/colorbox/jquery.colorbox.min.js')}}"></script>
        <!-- responsive image grid -->
            <script src="{{asset('asset2/js/lib/wookmark/jquery.imagesloaded.min.js')}}"></script>
                    <!-- responsive image grid -->
            <script src="{{asset('asset2/js/lib/wookmark/jquery.imagesloaded.min.js')}}"></script>
            <script src="{{asset('asset2/js/lib/wookmark/jquery.wookmark.min.js')}}"></script>

            <script src="{{asset('asset2/js/pages/beoro_gallery.js')}}"></script>
            <script src="{{asset('asset2/js/pages/beoro_tables.js')}}"></script>
          

    </body>
</html>