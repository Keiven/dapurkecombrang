<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        @include('admin.template.head')
    </head>
    <body class="bg_d">
        @include('admin.template.navbar')
            
        <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box" id="n_fileupload">
                            <div class="w-box-header">
                                <h4>Upload Media</h4>
                            </div>
                            <div class="w-box-content cnt_a">
                                <div class="row-fluid">
                                <form method="post" enctype="multipart/form-data">
                                    <div class="span6">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <label>Embed Video</label>
                                            <input type="text" placeholder="Paste Video Link" class="span8" name="embed" value="{{$media->MediaVideo}}">
                                        </div>
                                            <label>Media Title</label>
                                            <input type="text" placeholder="Type something…" class="span8" name="title" value="{{$media->MediaTittle}}">
                                            <label>Article</label>
                                        <div class="w-box" id="n_wysiwg">
                                            <div class="w-box-content cnt_no_pad">
                                                <textarea id="wysiwg_editor" cols="30" rows="10" name="content">{{$media->MediaContent}}</textarea>
                                            </div>
                                        </div>
                                        <label>Tag</label>
                                        <input type="text" id="s2_tag_handler" class="span12" value="{{{$media->MediaTags}}}" name="tags">
                                        <span class="help-block">Try to enter a space or a comma after tag.</span>

                                        <div class="sepH_b">
                                             <p style="color:#F00; font-size:15px;">{{Session::get('message')}}</p>
                                            <button type="submit" class="btn">Publish</button>
                                        </div>
                                        
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
   @include('admin.template.js')


    </body>
</html>