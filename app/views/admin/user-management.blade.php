<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        @include('admin.template.head')
    </head>
    <body class="bg_d">
   
        @include('admin.template.navbar')
            
        <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <div class="btn-group">
                                    <a href="#" class="btn btn-inverse btn-mini delete_rows_simple" data-tableid="smpl_tbl" title="Edit">Delete</a>
                                </div>
                            </div>
                            <div class="w-box-content">
                                <table class="table table-vam table-striped" id="smpl_tbl">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Image</th>
                                            <th>User</th>
                                            <th>Status</th>
                                            <th>Last Seen</th>
                                            <th>Last Login IP</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($userlist as $_userlist)
                                        <tr>
                                            <td>{{$_userlist['UserId']}}</td>
                                            <td style="width:20px;">
                                                <a href="" title="{{$_userlist['FullName']}}" class="thumbnail">
                                                    @if($_userlist['UserPhoto'] != '')
                                                    <img src="{{asset($_userlist['UserPhoto'])}}" style="height:20px;width:20px">
                                                    @else
                                                    <img src="{{asset('asset2/img/dummy_60x60.gif')}}" style="height:20px;width:20px">
                                                    @endif
                                                </a>
                                            </td>
                                            <td>{{$_userlist['Username']}}</td>
                                            <td>@if($_userlist['UserLevel'] > 1) {{'Admin'}} @else {{'Super Admin'}} @endif</td>
                                            @if($_userlist['LastLogin'] != '0000-00-00')
                                            <td>{{$_userlist['LastLoginTime'].' '.date("d-M-Y", strtotime($_userlist['LastLogin']))}}</td>
                                            @else
                                            <td>Never Login</td>
                                            @endif
                                            @if($_userlist['LastLoginIp'] != '')
                                            <td>{{$_userlist['LastLoginIp']}}</td>
                                            @else
                                            <td>Never Login</td>
                                            @endif
                                            <td> 
                                                <div class="btn-group">
                                                    <a href="{{asset('adm/user/profile/'.$_userlist['UserId'])}}" class="btn btn-mini" title="Edit"><i class="icon-pencil"></i></a>
                                                    <a href="{{asset('adm/user/profile/'.$_userlist['UserId'].'/delete')}}" class="btn btn-mini" title="Delete"><i class="icon-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="w-box-footer">
                                {{$userlist->links('admin.template.custom-paginate')}}
                            </div>
                        </div>
                                               
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Add New User</h4>
                            </div>
                            <div class="w-box-content">
                                <form method="post" enctype="multipart/form-data">
                                    <div class="formSep">
                                        <label>Avatar</label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 60px; height: 60px;"><img src="{{asset('asset2/img/dummy_60x60.gif')}}" alt="" ></div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="width: 60px; height: 60px;"></div>
                                            <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" name="photo"></span>
                                            <a href="#" class="btn btn-small btn-link fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                    <div class="formSep">
                                        <label for="u_username">Username</label>
                                        <input type="text" id="u_username" name="username" class="span6" value="{{Input::old('username')}}" />
                                    </div>
                                    <div class="formSep">
                                        <label for="u_name">Name</label>
                                        <input type="text" id="u_name" name="fullname" class="span6" value="{{Input::old('fullname')}}" />
                                    </div>
                                    <div class="formSep">
                                        <label for="u_password">Password</label>
                                        <input type="password" id="u_password" name="password" class="span6" />
                                        <span class="help-block">Enter Password</span>
                                        <input type="password" id="u_repassword" name="repassword" class="span6" />
                                        <span class="help-block">Repeat Password</span>
                                    </div>
                                    <div class="formSep">
                                        <label for="u_email">Email</label>
                                        <input type="text" id="u_email" name="email" class="span6" value="{{Input::old('email')}}" />
                                    </div>
                                    <div class="formSep">
                                        <label>User Level</label>
                                        <label for="u_level" class="radio inline"><input type="radio" name="level" id="u_level1" value="2" checked/> Admin</label>
                                        <label for="u_level" class="radio inline"><input type="radio" name="level" id="u_level2" value="1"/> Superadmin</label>
                                    </div>
                                    <div class="formSep">
                                        <label>Gender</label>
                                        <label for="u_male" class="radio inline"><input type="radio" name="gender" id="u_male" value="Male" checked/> Male</label>
                                        <label for="u_female" class="radio inline"><input type="radio" name="gender" id="u_female" value="Female"/> Female</label>
                                    </div>
                                    <div class="formSep">
                                        <label for="u_signature">Signature</label>
                                        <textarea name="signature" id="u_signature" cols="30" rows="4" class="span6">{{Input::old('signature')}}</textarea>
                                    </div>
                                    <div class="formSep sepH_b">
                                        <p style="color:#F00; font-size:15px;">{{Session::get('message')}}</p>
                                        <button class="btn btn-beoro-3" type="submit">Save changes</button>
                                        <a href="#" class="btn btn-link">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                        <!-- confirmation box -->
                        <div class="hide">
                            <div id="confirm_dialog" class="cbox_content">
                                <div class="sepH_c"><strong>Are you sure you want to delete this row(s)?</strong></div>
                                <div>
                                    <a href="#" class="btn btn-small btn-beoro-3 confirm_yes">Yes</a>
                                    <a href="#" class="btn btn-small confirm_no">No</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_space"></div>
        </div> 

    <!-- footer --> 
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span5">
                        <div>&copy; Quantum Home Appliances 2015</div>
                    </div>
                </div>
            </div>
        </footer>
        
        @include('admin.template.js')

    </body>
</html>