<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php $desc = MySetting::getDesc(); ?>
<!DOCTYPE HTML>
<html>
<head>
  @include('template.head')
</head>
<body>
<!--banner start here-->
 @include('template.banner-main', ['desc' => $desc])
<!--baner end here-->
<!--navgation start here-->
<!--baner end here-->
<!--navgation start here-->
  @include('template.navbar')



  @include('template.js')
<!--services start here-->
<div class="services">
	<div class="container">
		<div class="services-main">
			<div class="service-top">
				<h3>Services</h3>
				<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores</p>
			</div>
			<div class="service-bottom">
				<div class="col-md-4 service-left wow bounceInLeft" data-wow-delay="0.5s">
					<a href="single.html"><img src="asset/images/r5.jpg" alt="" class="img-responsive"></a>
				</div>
				<div class="col-md-8 services-right wow bounceInRight" data-wow-delay="0.5s">
					<h4><a href="single.html">Nam libero tempore soluta nobis  eligendi</a></h4>
					<p>Molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem  rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
				</div>
			  <div class="clearfix"> </div>
			</div>
		</div>
		<div class="addition">
				<h3>Additional Services</h3>
				<div class="addition-main">
					<div class="col-md-6 addition-left">
						<div class="cantin">
							<div class="col-md-3 cantin-icons">
								<span class="ser-icon1"> </span>
							</div>	
							<div class="col-md-9 cantin-text">
								<h4><a href="single.html">voluptatibus maiores perferendis</a></h4>
								<p>Doloribus repellat voluptatibus maiores alias consequatur aut perferendis doloribus asperiores.</p>
							</div>
						  <div class="clearfix"> </div>
						</div>
						<div class="cantin">
							<div class="col-md-3 cantin-icons">
								<span class="ser-icon2"> </span>
							</div>	
							<div class="col-md-9 cantin-text">
								<h4><a href="single.html">voluptatibus maiores perferendis</a></h4>
								<p>Doloribus repellat voluptatibus maiores alias consequatur aut perferendis doloribus asperiores.</p>
							</div>
						  <div class="clearfix"> </div>
						</div>
					</div>
					<div class="col-md-6 addition-right">
						<a href="single.html"><img src="asset/images/fruit.jpg" alt="" class="img-responsive"></a>
					</div>
				<div class="clearfix"> </div>
			</div>
	   </div>
	   <div class="feature wow bounceInRight" data-wow-delay="0.5s">
		  	<h3>Our Features</h3>
		  	<div class="feature-main">
		  		<div class="col-md-4 fact-grid">
		  			<div class="col-md-2 cord-drop">
		  				<span class="numbers">1</span>
		  			</div>
		  			 <div class="col-md-10 feature-text">
			  				<h4><a href="single.html">There are variations passage</a></h4>
			  				<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain</p>
		  			 </div>	
		  		  <div class="clearfix"> </div>
		  		</div>
		  		<div class="col-md-4 fact-grid">
		  			<div class="col-md-2 cord-drop">
		  				<span class="numbers">2</span>
		  			</div>
		  			 <div class="col-md-10 feature-text">
			  				<h4><a href="single.html">There are variations passage</a></h4>
			  				<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain</p>
		  			 </div>	
		  		  <div class="clearfix"> </div>
		  		</div>
		  		<div class="col-md-4 fact-grid">
		  			<div class="col-md-2 cord-drop">
		  				<span class="numbers">3</span>
		  			</div>
		  			 <div class="col-md-10 feature-text">
			  				<h4><a href="single.html">There are variations passage</a></h4>
			  				<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain</p>
		  			 </div>	
		  		  <div class="clearfix"> </div>
		  		</div>
		  		<div class="clearfix"> </div>
		   </div>
       </div>
	</div>
</div>
<!--services end here-->
<!--footer start here-->
@include('template.footer',['desc' => $desc])
<!--footer end here-->

</body>
</html>