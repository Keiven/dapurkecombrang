<!DOCTYPE html>
<html lang="en">
<head>
@include('template.head')
</head>

<body>
<!--==============================
              header
=================================-->
<header>
  @include('template.navbar')
</header>
<!--=====================
          Content
======================-->
<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - August11, 2014!</div>
  <div class="container">
    <div class="row">
      <div class="grid_12">
        <h2>About Attaya Futsal</h2>
          <img src="{{$about->AboutImage}}" alt="" class="img_inner fleft">
          {{$about->AboutContent}}
      </div>
    </div>
  </div>
  <div class="sep-1 offset__1"></div>
  <div class="container">
    <div class="row">
      <div class="grid_12">
        <h3>My Recent  Awards</h3>
      </div>
      <div class="grid_3">
        <div class="block-1">
          <div class="fa fa-image"></div>
          <div class="block-1_title">Aliquam nibh ante</div>Dorem ipsum dolor sit amet, consectetur adipiscing elit. In mollis erat mattis neque facilisis, sit amet ultricies erat rutrum. Cras facilisis, nulla vel viverra auctor, leo magna sodales felis, quis malesuada nibh odio ut velit
          <br>
          <a href="#" class="btn">more</a>
        </div>
      </div>
      <div class="grid_3">
        <div class="block-1">
          <div class="fa fa-trophy"></div>
          <div class="block-1_title">Kiam nibh anteli</div>Dorem ipsum dolor sit amet, consectetur adipiscing elit. In mollis erat mattis neque facilisis, sit amet ultricies erat rutrum. Cras facilisis, nulla vel viverra auctor, leo magna sodales felis, quis malesuada nibh odio ut velit
          <br>
          <a href="#" class="btn">more</a>
        </div>
      </div>
      <div class="grid_3">
        <div class="block-1">
          <div class="fa fa-star"></div>
          <div class="block-1_title">Nomilonibh anter</div>Dorem ipsum dolor sit amet, consectetur adipiscing elit. In mollis erat mattis neque facilisis, sit amet ultricies erat rutrum. Cras facilisis, nulla vel viverra auctor, leo magna sodales felis, quis malesuada nibh odio ut velit
          <br>
          <a href="#" class="btn">more</a>
        </div>
      </div>
      <div class="grid_3">
        <div class="block-1">
          <div class="fa fa-camera"></div>
          <div class="block-1_title">Moliuam nibh anteg</div>Dorem ipsum dolor sit amet, consectetur adipiscing elit. In mollis erat mattis neque facilisis, sit amet ultricies erat rutrum. Cras facilisis, nulla vel viverra auctor, leo magna sodales felis, quis malesuada nibh odio ut velit
          <br>
          <a href="#" class="btn">more</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!--==============================
              footer
=================================-->
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="grid_12"> 
        <h2>Contacts</h2>
        <div class="footer_phone">+1 800 559 6580</div>
        <a href="#" class="footer_mail">info@demolink.org</a>
        <div class="sub-copy">Website designed by <a href="http://www.templatemonster.com/" rel="nofollow">TemplateMonster.com</a></div>
      </div>
    </div>

  </div>  
</footer>
<a href="#" id="toTop" class="fa fa-chevron-up"></a>
</body>
@include('template.js')
</html>

