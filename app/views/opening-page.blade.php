<!DOCTYPE html>
<html class="no-js" lang="en-US">  <!--<![endif]-->
    <head>
        <!-- stylesheets -->
        <link rel="stylesheet" href="http://quantum.dev/asset/css/basic.css" />
        <link rel="stylesheet" href="http://quantum.dev/asset/css/style.css" />   
        <link rel="stylesheet" href="http://quantum.dev/asset/css/responsive.css" />  
        <link rel="stylesheet" href="http://quantum.dev/asset/css/yellow.css" />
        <link rel="stylesheet" href="http://quantum.dev/asset/css/prettyPhoto.css" media="screen" />

        <link rel="stylesheet" href="http://quantum.dev/asset/css/responsive.css" />  
        <link rel="stylesheet" href="http://quantum.dev/asset/css/yellow.css" />
        <link rel="stylesheet" href="http://quantum.dev/asset/css/nivo-slider.css" />
        <link rel="stylesheet" href="http://quantum.dev/asset/js/jplayer/skin/pixel-industry/pixel-industry.css" />

        <!-- revolution slider settings -->
        <link rel="stylesheet" type="text/css" href="http://quantum.dev/asset/rs-plugin/css/settings.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="http://quantum.dev/asset/rs-plugin/css/revolution.css" media="screen" />

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic' rel='stylesheet' type='text/css'>

        <!-- Icons -->
        <link rel="stylesheet" href="http://quantum.dev/asset/pixons/style.css" />
        <link rel="stylesheet" href="http://quantum.dev/asset/font-awesome/css/font-awesome.min.css" />
        <!-- CSS Global Compulsory -->
        <link rel="stylesheet" href="asset/css/bootstrap.min.css">
        <link rel="stylesheet" href="asset/style.css" >
        
        <!-- CSS Implementing Plugins -->
        <link rel="stylesheet" href="asset/css/jquery.fullPage.css">
        <link rel="stylesheet" href="asset/css/font-awesome.min.css">
        <link rel="stylesheet" href="asset/css/ionicons.min.css">
        <link rel="stylesheet" href="asset/css/animate.min.css">
        <link rel="stylesheet" href="asset/css/flexslider.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        
        <!--[if lt IE 11]>
            <link rel="stylesheet" type="text/css" href="css/ie.css">
        <![endif]-->
        
        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,700italic,400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,700,900' rel='stylesheet' type='text/css'>
        
        <!-- JS -->
        <script type="text/javascript" src="asset/js/modernizr.js"></script>
        
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="asset/images/favicon.ico">
        <link rel="apple-touch-icon" href="asset/images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="asset/images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="asset/images/apple-touch-icon-114x114.png">
    
    </head>
    <body>
    
        <!-- START PRELOADER -->
        <div id="preloader">
            <div id="loading-animation">&nbsp;</div>
        </div>
        <!-- END PRELOADER -->
        
        <!-- START SITE HEADER -->
        <header class="site-header">
        </header>
        <!-- END SITE HEADER -->

        <div id="fullpage">
                            
            <!-- HOME SECTION -->
            <div class="section home active cover-background" style="background-image: url({{asset($url->OpeningPhoto)}});" id="section0">
                <!-- START CONTAINER -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <!-- START SLIDE CONTENT-->
                            <div class="slide-content">
                                <!-- <div class="flexslider textslider onstart animated" data-animation="fadeInUp" data-animation-delay="800">
                                    <ul class="slides">
                                        <li>
                                            <h1 class="rotate uppercase">We're <span class="font-normal">Creative<br>Studio</span> from Sydney</h1>
                                        </li>
                                    </ul>
                                </div>
                                <p class="lead onstart animated" data-animation="fadeInUp" data-animation-delay="1000">Our Website Is Coming Soon</p> -->
                                <a href="/home" class="border-button bt-border-white go-slide onstart animated" data-animation="fadeInUp" data-animation-delay="1200" data-slide="2">Enter Our Site</a>
                            </div><!-- END SLIDE CONTENT-->
                        </div>
                    </div><!-- END ROW -->
                </div><!-- END CONTAINER -->
                <!-- OVERLAY -->
                <div class="overlay">
                    <div class="gradient-overlay background-blue-dark opacity-40"></div>
                </div>
            </div>              
        </div>
        
        <!-- START SOCIAL ICONS -->
        <nav class="socials-icons">
            <ul>
                <li class="onstart animated" data-animation="fadeInRightBig" data-animation-delay="800"><a href="#"><i class="ion-social-facebook"></i></a></li>
                <li class="onstart animated" data-animation="fadeInRightBig" data-animation-delay="950"><a href="#"><i class="ion-social-twitter"></i></a></li>
            </ul> 
        </nav>
        <!-- END SOCIAL ICONS -->
        
        <!-- START FOOTER -->
        <footer class="site-footer onstart animated" data-animation="fadeInUp" data-animation-delay="800">
            <p class="copyright">© 2015 Quantum Home Appliances - All Rights Reserved</p>
        </footer>
        <!-- END FOOTER -->
                
        <!-- JS -->
        <script type="text/javascript" src="asset/js/jquery.min.js"></script>
        <script type="text/javascript" src="asset/js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="asset/js/plugins.min.js"></script>
    
        <script type="text/javascript" src="asset/js/main.js"></script>
        
    </body>
</html>