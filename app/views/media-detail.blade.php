<!DOCTYPE html>
<html>
    <head>
		@include('template.head')
    </head>

    <body>
        
        @include('template.navbar')

        <!-- .top-shadow -->
        <div class="top-shadow"></div>

        <!-- .page-title-container start -->
        <section class="page-title-container">

            <!-- .container_12 start -->
            <div class="container_12">

                <!-- .page-title start -->
                <div class="page-title grid_12">
                    <div class="title">
                        <h1>Recent Media</h1>
                    </div>

                    <ul class="breadcrumbs">
                        <li><a  class="home" href="#">Home</a></li>
                        <li>/</li>
                        <li><span class="active">News</span></li>
                    </ul>
                </div><!-- .page-title end -->
            </div><!-- .container_12 end -->
        </section><!-- .page-title-container end -->

        <!-- #content-wrapper start -->
        <section id="content-wrapper">
            <div class="container_12">

                <!-- .grid_8 .blog-posts start -->
                <ul class="grid_8 blog-posts">

                    <!-- .blog-post .format standard sticky  start -->
                    <li class="blog-post format-standard clearfix">
                        
                        <!-- .blog-meta start -->
                        <section class="blog-meta">
                            <div class="category">
                                <i class="icon-edit"></i>
                            </div>

                            <ul>
                                <li>
                                    <span class="text-dark">Posted: </span>Apr 28, 2013
                                </li>

                                <li>
                                    <span class="text-dark">By: </span>
                                    <a href="#">{{$media->MediaWriter}}</a>
                                </li>
                            </ul>
                        </section><!-- .blog-meta end -->

                        <!-- post-body-container start -->
                        <article class="post-body-container">
                            <?php $link = asset('media/'.$media->MediaId.'/'.str_replace(' ','-',strtolower($media->MediaTittle))) ?>
                            <!-- .post-body start -->
                            <section class="post-body">
                                {{Embed::convert($media->MediaVideo)}}

                                <a href="{{$link}}">
                                    <h3>{{$media->MediaTittle}}</h3>
                                </a>

                                <ul class="tags">
                                    <li>
                                        <span class="text-dark">Tags: </span>
                                    </li>
                                    <?php 
                                        $tag = explode(",",$media->MediaTags); 
                                        $limit = count($tag);
                                        $i=0;
                                    ?>
                                    @foreach($tag as $_tag)
                                    @if(++$i != $limit)
                                    <li>
                                        <a href="{{asset('media/tag/'.$_tag)}}">{{$_tag.','}} </a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="{{asset('media/tag/'.$_tag)}}">{{$_tag}} </a>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>

                                {{$media->MediaContent}}
                                
                            </section><!-- .post-body end -->

                            <!-- .share-post start -->
                            <article class="share-post">
                                <span>Share this post:</span>
                                <div id="shareme" data-url="http://sharrre.com/"></div>
                            </article><!-- share-post end -->
                        </article><!-- post-body-container end -->

                    </li><!-- .blog-post .standard .stiky end -->
                </ul><!-- .grid_8 .blog-posts end -->

                <!-- aside right start -->
                <aside class="grid_4 aside-right">

                    <!-- .widget start -->
                    <ul class="aside_widgets">

                        <!-- .widget_recent_entries start -->
                        <li class="widget pi_recent_posts">
                            <div class="title">
                                <h5>latest news</h5>
                            </div>

                            <ul class="footer-blog">
                                @foreach($latest as $_latest)
                                 <?php $link = asset('media/'.$_latest->MediaId.'/'.str_replace(' ','-',strtolower($_latest->MediaTittle))) ?>
                                <li>
                                    <div class="meta">
                                        <a class="icon-edit"></a>
                                    </div>

                                    <div class="post">
                                        <a href="{{$link}}"> {{{$_latest->MediaTittle}}}</a>
                                        <p class="info">{{date("M d, Y", strtotime($_latest->created_at))}}</p>
                                    </div>
                                </li>
                                @endforeach
                        </li><!-- .widget_recent_entries end -->

                    </ul><!-- .aside_widgets end -->
                </aside> <!-- aside right end -->
            </div><!-- .container_12 end -->
        </section><!-- #content-wrapper end -->

        <!-- #footer-wrapper start -->
        @include('template.footer')
        
        <!-- scripts -->
        <script  src="{{asset('asset/js/jquery-1.8.3.js')}}"></script> <!-- jQuery library -->  
        <script  src="{{asset('asset/js/jquery.placeholder.min.js')}}"></script><!-- jQuery placeholder fix for old browsers -->
        <script  src="{{asset('asset/js/jquery.carouFredSel-6.0.0-packed.js')}}"></script><!-- CarouFredSel carousel plugin -->
        <script  src="{{asset('asset/sharre/jquery.sharrre-1.3.4.min.js')}}"></script>
        <script  src="{{asset('asset/js/jquery.touchSwipe-1.2.5.js')}}"></script><!-- support for touch swipe on mobile devices -->
        <script  src="{{asset('asset/js/include.js')}}"></script> <!-- jQuery custom options -->
        <script  src="{{asset('asset/js/nv.js')}}"></script>

        <script>
            /* <![CDATA[ */
            // NEWSLETTER FORM AJAX SUBMIT
            $('.newsletter .submit').on('click', function(event){
                event.preventDefault();
                var email = $('.email').val();
                var form_data = new Array({ 'Email' : email});
                $.ajax({
                    type: 'POST',
                    url: "contact.php",
                    data: ({'action': 'newsletter', 'form_data' : form_data})
                }).done(function(data) {
                    alert(data);
                });
            });
            
            // COMMENT FORM AJAX SUBMIT
            $('#commentform #comment-reply').on('click', function(event){
                event.preventDefault();
                var name = $('#comment-name').val();
                var email = $('#comment-email').val();
                var message = $('#comment-message').val();
                var form_data = new Array({'Name' : name, 'Email' : email, 'Message' : message});
                $.ajax({
                    type: 'POST',
                    url: "contact.php",
                    data: ({'action': 'comment', 'form_data' : form_data})
                }).done(function(data) {
                    alert(data);
                });
            });

            //JQUERY SHARRE PLUGIN
            $('#shareme').sharrre({
                share: {
                    twitter: true,
                    facebook: true,
                    googlePlus: true
                },
                template: '<div class="box"><div class="left">Share</div><div class="middle"><a href="#" class="facebook">f</a><a href="#" class="twitter">t</a><a href="#" class="googleplus">+1</a></div><div class="right">{total}</div></div>',
                enableHover: false,
                enableTracking: true,
                render: function(api, options) {
                    $(api.element).on('click', '.twitter', function() {
                        api.openPopup('twitter');
                    });
                    $(api.element).on('click', '.facebook', function() {
                        api.openPopup('facebook');
                    });
                    $(api.element).on('click', '.googleplus', function() {
                        api.openPopup('googlePlus');
                    });
                }
            });

            // TESTIMONIAL CAROUSEL
            $("#foo2").carouFredSel({
                auto: true,
                scroll: 1,
                swipe: {
                    ontouch: true,
                    onMouse: true
                },
                width: 'variable',
                height: 'variable',
                items:{
                    width: 'auto',
                    visible: 1
                }
            });


            // FOOTER CAROUSEL ARTICLE 
            $("#foo1").carouFredSel({
                auto: true,
                scroll: 1,
                pagination: "#foo1_pag",
                swipe: {
                    ontouch: true,
                    onMouse: true
                },
                width: 'variable',
                height: 'variable',
                items: {
                    width: 'auto',
                    visible: 1
                }
            });
            /* ]]> */
        </script>
    </body>
</html>