<!DOCTYPE html>
<html>
    <head>
        @include('template.head')
        
    </head>

    <body>
        
        @include('template.navbar')

        <!-- .top-shadow -->
        <div class="top-shadow"></div>

        <!-- .page-title-container start -->
        <section class="page-title-container">

            <!-- .container_12 start -->
            <div class="container_12">

                <!-- .page-title start -->
                <div class="page-title grid_12">
                    <div class="title">
                        <h1>Product Detail</h1>
                    </div>

                    <ul class="breadcrumbs">
                        <li><a  class="home" href="#">Home</a></li>
                        <li>/</li>
                        <li><span class="active">Product Detail</span></li>
                    </ul>
                </div><!-- .page-title end -->
            </div><!-- .container_12 end -->
        </section><!-- .page-title-container end -->

        <!-- #content-wrapper start -->
        <section id="content-wrapper">

            <!-- .container_12 start -->
            <section class="container_12 clearfix">

                <!-- .grid_7.slider-wrapper start -->
                <article class="grid_7 slider-wrapper">
                    <section id="slider" class="nivoSlider image-slider portfolio-slider">
                        <img src="{{asset($product->ProductImage)}}" alt="{{'Quantum '. $product['CategoryName']. ' ' .$product['ProductName']}}" title="#htmlcaption1"/>
                    </section>
                </article><!-- .grid_7.slider-wrapper end -->

                <!-- .grid_5.portfolio-description start -->
                <article class="grid_5 portfolio-description">
                    <h3>{{$product->ProductName}}</h3>

                    <ul class="meta">
                        <li>
                            <span>
                                {{$product->CategoryName}} 
                            </span>
                            //
                        </li>

                        <li class="date">{{date("d M Y", strtotime($product->created_at))}}</li>
                    </ul>

                    <p>
                        {{$product->ProductDesc}}

                    </p>

                    <br />

                </article><!-- .grid_5.portfolio-description end -->
            </section><!-- .container_12 end -->
        </section><!-- #content-wrapper end -->

        @include('template.footer')

        <!-- scripts -->
        <script  src="{{asset('asset/js/jquery-1.8.3.js')}}"></script> <!-- jQuery library -->  
        <script  src="{{asset('asset/js/jquery.placeholder.min.js')}}"></script><!-- jQuery placeholder fix for old browsers -->
        <script  src="{{asset('asset/js/jquery.carouFredSel-6.0.0-packed.js')}}"></script><!-- CarouFredSel carousel plugin -->
        <script  src="{{asset('asset/js/portfolio.js')}}"></script> <!-- portfolio custom options -->
        <script  src="{{asset('asset/js/quicksand.js')}}"></script> <!-- quicksand filter -->
        <script  src="{{asset('asset/js/jquery.nivo.slider.js')}}"></script><!-- nivo slider -->
        <script  src="{{asset('asset/js/jquery.prettyPhoto.js')}}"></script> <!-- prettyPhoto lightbox -->
        <script  src="{{asset('asset/js/jquery.touchSwipe-1.2.5.js')}}"></script><!-- support for touch swipe on mobile devices -->
        <script  src="{{asset('asset/js/include.js')}}"></script> <!-- jQuery custom options -->
        <script  src="{{asset('asset/js/nv.js')}}"></script>
        

        <script>
            /* <![CDATA[ */
            // NEWSLETTER FORM AJAX SUBMIT
            $('.newsletter .submit').on('click', function(event){
                event.preventDefault();
                var email = $('.email').val();
                var form_data = new Array({ 'Email' : email});
                $.ajax({
                    type: 'POST',
                    url: "contact.php",
                    data: ({'action': 'newsletter', 'form_data' : form_data})
                }).done(function(data) {
                    alert(data);
                });
            });
            
            /* NIVO SLIDER */
            $(window).load(function() {
                $('#slider').nivoSlider();
            });

            // PORTFOLIO CAROUSEL
            $("#foo2").carouFredSel({
                auto: false,
                scroll: 1,
                pagination: "#foo2_pag",
                width: 'variable',
                height: 'variable',
                items:{
                    width: 'auto',
                    visible: 4
                }
            });

            // FOOTER CAROUSEL ARTICLE 
            $("#foo1").carouFredSel({
                auto: true,
                scroll: 1,
                pagination: "#foo1_pag",
                swipe: {
                    ontouch: true,
                    onMouse: true
                },
                width: 'variable',
                height: 'variable',
                items:{
                    width: 'auto',
                    visible: 1
                }
            });
            /* ]]> */
        </script>
    </body>
</html>
