<?php $desc = MySetting::getDesc(); ?>
<!DOCTYPE HTML>
<html>
<head>
    @include('template.head')
    <link href="css/style.css" rel="stylesheet" type="asset/text/css" media="all"/>
    <link rel="stylesheet" type="text/css" href="asset/css/style_common.css" />
    <link rel="stylesheet" type="text/css" href="asset/css/style7.css" />
</head>
<body>
<!--banner start here-->
 @include('template.banner-main', ['desc' => $desc])
<!--baner end here-->
<!--navgation start here-->
  @include('template.navbar')



  @include('template.js')

<!--recipies start here-->
<div class="recipes">
    <div class="container">
        <div class="recipes-main">
            <div class="recipes-top wow bounceInLeft" data-wow-delay="0.5s">
                <h3>Galeri</h3>
                <p>{{$desc->DescGallery}}</p>
            </div>
            <div class="recipes-bottom">
                @foreach($gallery as $_gallery)
               <div class="view view-seventh">
                    <a href="{{asset($_gallery->GalleryImage)}}" class="b-link-stripe b-animate-go  swipebox"  title="Image Title">
                    <img src="{{asset($_gallery->GalleryImageThumb)}}" alt="{{'Dapur Kecombrang '.$_gallery->GalleryName}}" class="img-responsive">
                    <div class="mask">
                        <h2>{{$_gallery->GalleryName}}</h2>
                        <p>{{$_gallery->GalleryDesc}}</p>
                        <span class="gall">More</span>
                    </div></a>
                </div>
                @endforeach
             <div class="clearfix"> </div>
             </div>
        </div>
    </div>
</div>


<link rel="stylesheet" href="asset/css/swipebox.css">
    <script src="asset/js/jquery.swipebox.min.js"></script> 
        <script type="text/javascript">
            jQuery(function($) {
                $(".swipebox").swipebox();
            });
</script>

<!--recipies end here-->
<!--footer start here-->
<div class="footer">
    <div class="container">
        <div class="footer-main">
            <div class="col-md-4 footer-grid wow bounceIn" data-wow-delay="0.4s">
                <h3>About Us</h3>
                <ul>
                    <li><a href="index.html" class="active">Home</a></li>
                    <li><a href="menu.html">Menu</a></li>
                    <li><a href="services.html">Services</a></li>
                    <li><a href="typo.html">Typo</a></li>
                    <li><a href="recipies.html">Recipies</a></li>
                    <li><a href="contact.html">Contact</a></li>
                </ul>
            </div>
            <div class="col-md-4 footer-grid wow bounceIn" data-wow-delay="0.4s">
                <h3>Get Involved</h3>
                <p><a href="#">Occur in toli</a></p>
                <p><a href="#">Which pain can</a></p>
                <p><a href="#">Procure some</a></p>
                <p><a href="#">Great pleasure</a></p>
                <p><a href="#">Produce resultan</a></p>
            </div>
            <div class="col-md-4 footer-grid wow bounceIn" data-wow-delay="0.4s">
                <h3>Get in Touch</h3>
                <p>Address : Richard McClintock</p>
                <p>New Street : Letraset sheets</p>
                <p>ph : +123 859 6050</p>
            </div>
          <div class="clearfix"> </div>
        </div>
        <div class="copyright">
                <p>© 2015 Food Stuff All rights reserved | Design by  <a href="http://w3layouts.com/" target="_blank">  W3layouts </a></p>
        </div>
    </div>
</div>
<!--footer end here-->

</body>
</html>