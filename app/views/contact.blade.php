<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php $desc = MySetting::getDesc(); ?>
<!DOCTYPE HTML>
<html>
<head>
  @include('template.head')
</head>
<body>
  @include('template.banner-main', ['desc' => $desc])

  @include('template.navbar')



  @include('template.js')
<div class="contact">
  <div class="container">
    <div class="container-main">
      <div class="contact-top wow bounceInLeft" data-wow-delay="0.5s">
        <h3>Contact Us</h3>
        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium totam rem.</p>
      </div>
      <div class="contact-bottom wow bounceInRight" data-wow-delay="0.5s">
       <form>
        <input type="text" value="Name" class="name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}"/>
        <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}"/>
        <textarea onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
        <input type="submit" value="Send" />
      </form> 
      </div>
    <div class="clearfix"> </div>
  </div>
<div class="map">
     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387144.007583421!2d-73.97800349999999!3d40.7056308!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1415253431785"  frameborder="0" style="border:0"> </iframe>
  </div>
</div>
</div>
<!--footer start here-->
@include('template.footer',['desc' => $desc])
<!--footer end here-->

</body>
</html>