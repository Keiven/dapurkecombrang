<html>
<head>
	<title>Error</title>
	<link rel="stylesheet" href="{{asset('asset/css/error.css')}}"/>
</head>
<body>
	<div class="wrapper">
		<h1>404</h1>
		<h4>Oops, you've encountered an error</h4>
		<p>it appears the page you were looking for doesn't exist. sorry about that.</p>
	</div>
</body>
</html>