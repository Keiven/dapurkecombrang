<!DOCTYPE HTML>
<html>
<head>
  @include('template.head')
  <style>
      .banner {
      background: url({{$desc->DescBannerImage}})no-repeat;
      min-height: 900px;
      background-size: cover;
    }

    .main-strip {
      background: url({{$desc->DescBodyBannerImage}})no-repeat center;

      min-height: 1.6em;
      background-size: cover;
      padding: 5em 0em 5em 0em;
      text-align:center;
    }
  </style>
</head>
<body>
<!--banner start here-->

<div class="banner">
  <div class="header">
      <div class="container">
          <div class="header-main">
        <div class="logo wow bounceInLeft" data-wow-delay="0.5s">
          <a href="index.html"><img src="asset/images/lo.png" alt=""></a>
        </div>
        <div class="head-right wow bounceInRight" data-wow-delay="0.5s">
          <ul>
            <li><span class="phon"> </span><h4>{{{$desc->DescBannerPhone}}}</h4></li>
            <li><a href="mailto:infoexample@email.com"><span class="email"> </span>{{{$desc->DescBannerEmail}}}</a></li>
          </ul>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="banner-main">
        <h2>{{{$desc->DescBannerTitle}}}</h2>
        <p>{{{$desc->DescBannerSubtitle}}}</p>
        <div class="bann-btn">
          <a href="#">Lihat Menu</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!--baner end here-->
<!--navgation start here-->
  @include('template.navbar')



  @include('template.js')



<div class="working">
  <div class="container">
    <div class="working-main">
      <div class="working-top">
        {{$desc->DescWelcome}}
      </div>
      <div class="working-bottom">
        <div class="col-md-8 working-left wow bounceInLeft" data-wow-delay="0.5s">
          {{$about->AboutContent}}
            <a href="/menu">Lihat Menu</a>
        </div>
        <div class="col-md-4 working-right wow bounceInRight" data-wow-delay="0.5s">
          <a href="#"><img src="{{asset($about->AboutImage)}}" alt="dapur kecombrang {{$about->AboutContent}}" class="img-responsive"></a>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
</div>
<!--working end here-->
<!--we do grid start here-->
<div class="we-do">
  <div class="container">
    <div class="wedo-main">
      <div class="we-do-top">
        <h3>How It Works</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor aliqua.</p>
      </div>
      <div class="we-do-bottom">
        <div class="col-md-3 we-do-grid wow bounceIn" data-wow-delay="0.4s">
          <img src="asset/images/f1.png" alt="">
          <h4>Labore et dolore magna</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor aliqua.</p>
        </div>
        <div class="col-md-3 we-do-grid wow bounceIn" data-wow-delay="0.4s">
          <img src="asset/images/f2.png" alt="">
          <h4>Labore et dolore magna</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor aliqua.</p>
        </div>
        <div class="col-md-3 we-do-grid wow bounceIn" data-wow-delay="0.4s">
          <img src="asset/images/f3.png" alt="">
          <h4>Labore et dolore magna</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor aliqua.</p>
        </div>
        <div class="col-md-3 we-do-grid wow bounceIn" data-wow-delay="0.4s">
          <img src="asset/images/f4.png" alt="">
          <h4>Labore et dolore magna</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor aliqua.</p>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</div>
<!--we do grid end here-->
<!--main starip statr here-->
<div class="main-strip">
  <div class="container">
    <div class="main-strip-start">
      <h3>{{{$desc->DescBodyBannerTitle}}}</h3>
      <a href="/menu">Lihat Lanjut</a>
    </div>
  </div>
</div>
<!--main strip end here-->
<!--food info  start here-->
<div class="food-info">
  <div class="container">
    <div class="food-info-main">
      @foreach($product as $_product)
      <div class="col-md-4 food-info-grids wow bounceIn" data-wow-delay="0.4s">
        <a href="/menu"><img src="{{asset($_product->ProductImageThumb)}}" alt="" class="img-responsive"></a>
        <div class="food-details">
          <h4><a href="/menu">{{$_product->ProductName}}</a></h4>
          {{$_product->ProductDesc}}
        </div>
      </div>
      @endforeach
      
      <div class="clearfix"> </div>
    </div>
  </div>
</div>
<!--food info  end here-->
<!--footer start here-->
@include('template.footer',['desc' => $desc])
<!--footer end here-->

</body>



</html>