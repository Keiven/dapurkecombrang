<div class="footer">
  <div class="container">
    <div class="footer-main">
      <div class="col-md-4 footer-grid  wow bounceIn" data-wow-delay="0.4s">
        <h3>About Us</h3>
        <ul>
          <li><a href="/home" class="active">Home</a></li>
          <li><a href="/menu">Menu</a></li>
          <li><a href="/home">Layanan</a></li>
          <li><a href="/gallery">Galeri</a></li>
          <li><a href="/contact">Kontak</a></li>
        </ul>
      </div>
      <div class="col-md-4 footer-grid  wow bounceIn" data-wow-delay="0.4s">
        <h3>Get Involved</h3>
        <p><a href="#">Occur in toli</a></p>
        <p><a href="#">Which pain can</a></p>
        <p><a href="#">Procure some</a></p>
        <p><a href="#">Great pleasure</a></p>
        <p><a href="#">Produce resultan</a></p>
      </div>
      <div class="col-md-4 footer-grid  wow bounceIn" data-wow-delay="0.4s">
        <h3>Alamat</h3>
        {{$desc->DescFooterBannerAddress}}
      </div>
      <div class="clearfix"> </div>
    </div>
    <div class="copyright">
        <p>© 2015 Dapur Kecombrang All rights reserved | Design by  <a href="http://w3layouts.com/" target="_blank">  W3layouts </a></p>
    </div>
  </div>
</div>