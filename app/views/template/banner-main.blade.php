<style>
.bann-two{
  background: url({{$desc->DescBannerImage}})no-repeat;
  min-height: 370px;
  background-size: cover;
}
</style>


<div class="bann-two">
    <div class="header">
            <div class="container">
              <div class="header-main">
                <div class="logo wow bounceInLeft" data-wow-delay="0.5s">
                    <a href="index.html"><img src="asset/images/lo.png" alt=""></a>
                </div>
                <div class="head-right wow bounceInRight" data-wow-delay="0.5s">
                    <ul>
                        <li><span class="phon"> </span><h4>{{$desc->DescBannerPhone}}</h4></li>
                        <li><a href="mailto:infoexample@email.com"><span class="email"> </span>{{$desc->DescBannerEmail}}</a></li>
                    </ul>
                </div>
              <div class="clearfix"> </div>
            </div>
            <div class="bann-two-main">
                <h2>{{$desc->DescBannerTitle}}</h2>
            </div>
        </div>
    </div>
</div>