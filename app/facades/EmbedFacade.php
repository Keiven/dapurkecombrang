<?php namespace App\Facades;
 
use Illuminate\Support\Facades\Facade;
 
class EmbedFacade extends Facade {
 
    protected static function getFacadeAccessor()
    {
        return new \App\Services\Embed;
    }
 
}