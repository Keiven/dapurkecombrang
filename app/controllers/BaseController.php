<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function getip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	public function initVisitor()
	{
		if(!Session::has('visitor'))
		{
			$visitor					= new Analytic;
			$visitor->AnalyticAddress 	= $this->getip();
			$visitor->AnalyticPageView	= 1;
			$visitor->save();
			Session::put('visitor', $visitor->AnalyticId);
		}
		else
		{
			$visitor 					= Analytic::find(Session::get('visitor'));
			$visitor->AnalyticPageView+=1;
			$visitor->save();
		}
	}


	public function videoType($url) {
	    if (strpos($url, 'youtube.com/watch?v=') > 0) {
	        return 'youtube';
	    } elseif (strpos($url, 'vimeo.com/') > 0) {
	        return 'vimeo';
	    } else {
	        return 'unknown';
	    }
	}

	public function cleanInput($input) {
 
	  $search = array(
	    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
	    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
	  );
	 
	    $output = preg_replace($search, '', $input);
	    return $output;
	  }


	public function youtube($string) {
	    return preg_replace(
	        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
	        "<iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>",
	        $string
	    );
	}

	public function vimeo($string)
	{
		preg_match(
	        '/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/',
	        $string,
	        $matches
	    );

		$width  = 500;
		$height = 281;
	    $id = $matches[2];
	    return "<iframe src=\"https://player.vimeo.com/video/" .$id. "\" width=\"".$width."\" height=\"".$height."\" frameborder=\"0\" allowfullscreen></iframe>";
	}



}
