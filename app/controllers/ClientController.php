<?php

Class ClientController extends BaseController{
	/*==============================CLIENT VIEW CONTROLLER ZONE====================================*/

	public function showWelcome()
	{
		if(Setting::select('SettingOpeningPage')->find(1)->SettingOpeningPage != 1)return Redirect::to('/home');
		
		$this->initVisitor();
		$url = Opening::find(1);
		return View::make('opening-page',['url' => $url]);
	}

	public function GetServicesView()
	{
		$desc = Desc::find(1);
		return View::make('services', ['desc' => $desc]);
	}

	public function GetHomeView()
	{
		$this->initVisitor();

		//$slider 	= Slider::all();
		$about 		= About::find(1);
		$product 	= Product::orderBy(DB::raw('RAND()'))->take(3)->get();
		$desc  		= Desc::find(1);
		//$news   	= News::orderBy('NewsId','DESC')->take(4)->get();
		return View::make('index',['product' => $product, 'about' => $about, 'desc' => $desc ]);
	}

	public function GetProductView()
	{
		$this->initVisitor();
		$product = Product::all();
		$category = Product::select('CategoryName')->orderBy('CategoryName','DESC')->distinct()->get();
	
		return View::make('product',['product' => $product, 'category' => $category]);
	}

	public function GetProductDetailView($id ,$tittle)
	{
		$this->initVisitor();

		//$_tittle = str_replace('-',' ', $tittle);
		$product= Product::find($id);

		return View::make('product-detail',['product' => $product]);
	}

	public function GetGalleryView()
	{
		$this->initVisitor();
		$gallery = Gallery::orderBy('GalleryId','DESC')->paginate(9);
		try{
		if($gallery[0]->GalleryImage == null)
			echo '';
		}catch(exception $e)
		{	
			$gallery = array();
		}
		return View::make('gallery', ['gallery' => $gallery]);
	}

	public function GetNewsView()
	{
		$this->initVisitor();

		$news = News::orderBy('NewsId','DESC')->paginate(5);
		$latest = News::select('NewsTittle', 'created_at', 'NewsId')->orderBy('NewsId','DESC')->take(3)->get();
		return View::make('news',['news' => $news, 'latest' => $latest]);
	}

	public function GetNewsTagView($id)
	{
		$this->initVisitor();

		$news = News::where('NewsTags','like', '%'.$id.'%')->orderBy('NewsId','DESC')->paginate(5);
		$latest = News::select('NewsTittle', 'created_at', 'NewsId')->orderBy('NewsId','DESC')->take(3)->get();
		return View::make('news',['news' => $news, 'latest' => $latest]);
	}

	public function GetNewsDetailView($id ,$tittle)
	{
		$this->initVisitor();
		//$_tittle = str_replace('-',' ', $tittle);
		$news = News::find($id);
		if($news)
		{
			$latest = News::select('NewsTittle', 'created_at', 'NewsId')->orderBy('NewsId','DESC')->take(3)->get();
			return View::make('news-detail',['news' => $news, 'latest' => $latest]);
		}
		else
		{
			return Redirect::to('/');
		}
		
	}

	public function GetMediaView()
	{
		$this->initVisitor();

		$media = Media::orderBy('MediaId','DESC')->paginate(6);
		$latest = Media::select('MediaTittle', 'created_at', 'MediaId')->orderBy('MediaId','DESC')->take(3)->get();
		return View::make('media',['media' => $media, 'latest' => $latest]);
	}

	public function GetMediaTagView($id)
	{
		$this->initVisitor();

		$media = Media::where('MediaTags','like', '%'.$id.'%')->orderBy('MediaId','DESC')->paginate(5);
		$latest = Media::select('MediaTittle', 'created_at', 'MediaId')->orderBy('MediaId','DESC')->take(3)->get();
		return View::make('media',['media' => $media, 'latest' => $latest]);
	}

	public function GetMediaDetailView($id ,$tittle)
	{
		$this->initVisitor();

		//$_tittle = str_replace('-',' ', $tittle);
		$media = Media::find($id);
		if($media)
		{
			$latest = Media::select('MediaTittle', 'created_at', 'MediaId')->orderBy('MediaId','DESC')->take(3)->get();
			return View::make('media-detail',['media' => $media , 'latest' => $latest]);
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function GetContactView()
	{
		$this->initVisitor();

		return View::make('contact');
	}

	public function GetAboutView()
	{
		$this->initVisitor();

		$about = About::all();
		$partner = Partner::all();
		return View::make('about', ['about' => $about[0] , 'partner' => $partner]);
	}

	public function tes()
	{
		$data_weekly 			= Analytic::where('created_at', '>=', Carbon::now()->subWeek())
		                                  ->where('created_at', '<=', Carbon::now())->get();
		$data_weekly_before 	= Analytic::where('created_at', '>=', Carbon::now()->subWeek()->subWeek())
		                                  ->where('created_at', '<',  Carbon::now()->subWeek())->get();     
		echo Carbon::today() ;
	}
}