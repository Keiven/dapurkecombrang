<?php

use Carbon\Carbon;

class AdminController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	



	/*==============================ADMIN CONTROLLER ZONE====================================*/

	/*==================  JSON WEB ANALYTICS ==================*/
	public function json()
	{
		$a = new Carbon('now');
		$data = array();

		for($i = $a->startOfMonth()->day ; $i <= $a->endOfMonth()->day ; $i++)
		{
			$a->day = $i;
			$a->hour = 0;
			$a->minute = 0;
			$a->second = 0;
			$total_user = Analytic::remember(5)->select('AnalyticId')->where('created_at', '=',$a )->count();

			$data[date("m/d/Y",strtotime($a))] = $total_user;
		}
		return Response::json($data);
	}



	/*==================  LOGIN AREA ==================*/
	public function GetAdminLoginView()
	{
		if(Auth::check())return Redirect::to('adm/dashboard');
		else return View::make('admin.login');
	}

	public function GetAdminLogout()
	{
		Auth::logout();
		return Redirect::to('/adm/login');
	}

	public function GetAdminLoginAuth()
	{
		$userdata = array(
			'Username' => Input::get('username'),
			'password' => Input::get('password')
			);
		echo Input::get('login_remember');
		
		if(Input::get('login_remember') == 'on') $isAuth = Auth::attempt($userdata, true);
		else $isAuth = Auth::attempt($userdata);

		if($isAuth)
		{
			date_default_timezone_set("Asia/Jakarta");
			$user 					= Users::find(Auth::user()->UserId);
			$user->LastLoginIp   	= $this->getip();
			$user->LastLoginTime 	= date("h:i:sa");
			$user->LastLogin 	 	= date("Y-m-d H:i:s");
			$user->save();
			return Redirect::to('adm/dashboard');
		}
		else
		{ 
			return Redirect::to('adm/login')
				->with('message','User password is not valid, try again.');
		}
	}

	/*==================  DASHBOARD AREA ==================*/

	public function GetAdminDashboardView()
	{
		/*==========   ANALYTICS AREA ==========*/
		if(Carbon::today()->subMonth()->month == Carbon::now()->month)
			$monthly_before 	= Carbon::now()->subMonth()->subWeek()->startOfMonth();
		else
			$monthly_before 	= Carbon::now()->subMonth()->startOfMonth();

		$data_today 			= Analytic::select('AnalyticId')->where('created_at', '=',  Carbon::today())->count();
		$data_yesterday 		= Analytic::select('AnalyticId')->where('created_at', '=',  Carbon::yesterday())->count();
		$data_weekly 			= Analytic::select('AnalyticId')->where('created_at', '>=', Carbon::today()->subWeek())
		                                                        ->where('created_at', '<=', Carbon::today())->count();
		$data_weekly_before 	= Analytic::select('AnalyticId')->where('created_at', '>=', Carbon::today()->subWeek()->subWeek())
		                                                        ->where('created_at', '<',  Carbon::today()->subWeek())->count();                             
		$data_monthly 			= Analytic::select('AnalyticId')->where('created_at', '>=', Carbon::today()->startOfMonth())
		                                                        ->where('created_at', '<=', Carbon::today())->count();
		$data_monthly_before	= Analytic::select('AnalyticId')->where('created_at', '>=', $monthly_before)
		                                                        ->where('created_at', '<',  Carbon::today()->startOfMonth())->count();

		if($data_yesterday != 0){
			$today_percentage 	= (($data_today / $data_yesterday)-1)*100;
			if(is_float($today_percentage)) $today_percentage = number_format($today_percentage ,1); // 1 digit after point
		}
		else{
			$today_percentage = 0;
		}

		if($data_weekly_before != 0){
			$weekly_percentage 	= (($data_weekly / $data_weekly_before)-1)*100;
			if(is_float($weekly_percentage)) $weekly_percentage = number_format($weekly_percentage ,1);
		}
		else{
			$weekly_percentage 	= 0;
		}

		if($data_monthly_before != 0){
			$monthly_percentage = (($data_monthly / $data_monthly_before)-1)*100;
			if(is_float($monthly_percentage)) $monthly_percentage = number_format($monthly_percentage ,1);
		}
		else{
			$monthly_percentage = 0;
		}

		/*==========   END | ANALYTICS AREA ==========*/

		$userlist 		= Users::orderBy('UserId','asc')->paginate(10);
		
		return View::make('admin.dashboard', [
			'userlist' 				=> $userlist, 
			'data_today' 			=> $data_today, 
			'data_weekly' 			=> $data_weekly, 
			'data_monthly' 			=> $data_monthly,
			'today_percentage'		=> $today_percentage,
			'weekly_percentage'		=> $weekly_percentage,
			'monthly_percentage'	=> $monthly_percentage
		]);
	}

	/*==================  PRODUCT AREA ==================*/

	public function GetAdminProductView()
	{
		$desc 	 	= Desc::find(1);
		$category 	= Category::all();
		$product 	= Product::orderBy('ProductId','DESC')->paginate(10);
		return View::make('admin.product', ['category' => $category, 'product' => $product, 'desc' => $desc]);
	}

	public function PostAdminProductView()
	{
		$validator = Validator::make(Input::all() , array(
				'photo' 		=> 'image|required',
				'category'		=> 'required',
				'title'			=> 'required',
				'detail'		=> 'required'
			));
		if($validator->passes())
		{
			$filename = str_replace(' ','_',strtolower(Input::get('title'))).'.'.Input::file('photo')->getClientOriginalExtension();
			$location = base_path().'/asset/img/product/';
			Input::file('photo')->move($location  , $filename );

			$product = new Product;
			$product->CategoryName		= str_replace('-',' ',Input::get('category'));
			$product->ProductName 		= $this->cleanInput(Input::get('title'));
			$product->ProductDesc 		= $this->cleanInput(Input::get('detail'));
			$product->ProductImage 		= 'asset/img/product/'.$filename;
			$product->ProductPrice		= strtoupper($this->cleanInput(Input::get('price')));
			$product->ProductImageThumb = Image::thumbCustom('asset/img/product/',$filename, 370, 207);
			$product->save();

			
			
			return Redirect::back()->with('message','Add product success!');
		}
		else
		{
			return Redirect::back()->with('message','Something wrong, please fill up all the input form!');
		}
	}

	public function GetAdminProductEditView($id)
	{
		$category 	= Category::all();
		$product 	= Product::find($id);
		return View::make('admin.product-edit', ['category' => $category, 'product' => $product]);
	}

	public function PostAdminProductEditView($id)
	{
		$validator = Validator::make(Input::all() , array(
				'photo' 		=> 'image',
				'category'		=> 'required',
				'title'			=> 'required',
				'detail'		=> 'required'
			));
		if($validator->passes())
		{
			$product 							= Product::find($id);

			if(Input::file('photo') != '') //jika ketika edit foto ada
			{
				//hapus foto existinct
				File::delete($product->ProductImage,$product->ProductImageThumb);

				//set foto baru dan lokasi
				$filename = str_replace(' ','_',strtolower(Input::get('title'))).'.'.Input::file('photo')->getClientOriginalExtension();
				$location = base_path().'/asset/img/product/';
				Input::file('photo')->move($location  , $filename );

				//update ke database image
				$product->ProductImage 			= 'asset/img/product/'.$filename;
				$product->ProductImageThumb 	= Image::thumbCustom('asset/img/product/',$filename, 370, 207);
			}
			
			//update database
			$product->CategoryName				= str_replace('-',' ',Input::get('category'));
			$product->ProductName 				= $this->cleanInput(Input::get('title'));
			$product->ProductDesc 				= $this->cleanInput(Input::get('detail'));
			$product->ProductPrice				= strtoupper($this->cleanInput(Input::get('price')));
			//simpan databse
			$product->save();

			//balik ke halaman sebelumnya	
			return Redirect::back()->with('message','Edit product success!');
		}
		else
		{
			return Redirect::back()->with('message','Something wrong, please fill up all the input form!');
		}
	}

	public function GetAdminProductDelete($id)
	{
		$product = Product::find($id);
		File::delete($product->ProductImage,$product->ProductImageThumb);
		$product->delete();
		return Redirect::back();
	}

	/*==================  PRODUCT CATEGORY AREA ==================*/

	public function GetAdminProductCategoryAddView()
	{
		$category = Category::all();

		$totalproduct 	= array();

		foreach ($category  as $key) {
			array_push($totalproduct, Product::where('CategoryName', '=', $key->CategoryName)->count());
		}

		
		return View::make('admin.category-product', ['category' => $category, 'totalproduct' => $totalproduct]);
	}

	public function PostAdminProductCategoryAddView()
	{
		$data = array(
			'CategoryName'		=> Input::get('category')
		);
		$validator = Validator::make($data , array(
			'CategoryName'		=> 'required|unique:product_category',
		));
		if($validator->passes())
		{
			$category = new Category;
			$category->CategoryName = $this->cleanInput(Input::get('category'));
			$category->save();
			return Redirect::back()->with('message','Add category success!');
		}
		else
		{
			return Redirect::back()->with('message',$validator->messages()->first());
		}
	}


	public function PostAdminProductBannerEdit()
	{
		$desc = Desc::find(1);
		$desc->DescProduct = strip_tags(Input::get('desc'));
		$desc->save();
		return Redirect::back()->with('message2','Edit Menu description success!');
	}

	public function GetAdminProductCategoryEditView($id)
	{
		try{
			$id 		= Crypt::decrypt($id);
		}catch (Illuminate\Encryption\DecryptException $e){App::error($e,404);}

		$category 		= Category::where('CategoryName', '=', $id)->get();
		
		return View::make('admin.category-product-edit', ['category' => $category[0]]);		
	}

	public function PostAdminProductCategoryEditView($id)
	{
		try{
		$id = Crypt::decrypt($id);
		}catch (Illuminate\Encryption\DecryptException $e){
			//App::error($e,404);
		}

		$data = array(
			'CategoryName'		=> Input::get('category')
		);
		$validator = Validator::make($data , array(
			'CategoryName'		=> 'required|unique:product_category',
		));
		if($validator->passes())
		{
			Category::where('CategoryName', '=', $id)->update(array('CategoryName' => $this->cleanInput(Input::get('category'))));
			return Redirect::to('adm/menu/category/add');
		}
		else
		{
			return Redirect::back()->with('message',$validator->messages()->first());
		}

			
	}

	public function GetAdminProductCategoryDelete($id)
	{
		try{
		$id = Crypt::decrypt($id);
		}catch (Illuminate\Encryption\DecryptException $e){
			//App::error($e,404);
		}

		$product = Product::select('ProductImage','ProductImageThumb')->where('CategoryName', '=', $id)->get() ;
		foreach($product as $key){
			File::delete($key->ProductImage, $key->ProductImageThumb);
		}

		Category::where('CategoryName', '=', $id)->delete();
		return Redirect::to('adm/menu/category/add');	
	}
	

	


	/*==================  GALLERY AREA ==================*/

	public function GetAdminGalleryView()
	{
		$desc 	 = Desc::find(1);
		$gallery = Gallery::orderBy('GalleryId','DESC')->paginate(10);
		return View::make('admin.gallery',['gallery' => $gallery, 'desc' => $desc]);
	}

	
	public function PostAdminGalleryView()
	{
		$data = array(
				'photo'			=> Input::file('photo'),
				'GalleryName'	=> Input::get('title'),
				'subtitle'		=> Input::get('subtitle')
		);

		$validator = Validator::make($data , array(
				'photo' 		=> 'image|required',
				'subtitle'		=> 'required',
				'GalleryName'	=> 'required|unique:gallery'
		));

		if($validator->passes())
		{
			// Validate image size
			list($width , $height) = getimagesize(Input::file('photo'));
			if($width < 360 || $height < 260) return Redirect::back()->with('message','Image is too small !!');
			/*if width - height =  */
			$filename = str_replace(' ','_',strtolower(Input::get('title'))).'.'.Input::file('photo')->getClientOriginalExtension();
			$location = base_path().'/asset/img/gallery/';
			Input::file('photo')->move($location  , $filename );
			
			
			$gallery = new Gallery;
			$gallery ->GalleryName 			= strip_tags(Input::get('title'));
			$gallery ->GalleryDesc  		= strip_tags(Input::get('subtitle'));
			$gallery ->GalleryImage 		= 'asset/img/gallery/'.$filename;
			$gallery ->GalleryImageThumb 	= Image::thumbCustom('asset/img/gallery', $filename, 360, 260);
			$gallery ->save();
			return Redirect::back()->with('message','Add gallery success!');
		}
		else
		{
			return Redirect::back()->with('message',$validator->messages()->first());
		}
	}

	public function PostAdminGalleryBannerEdit()
	{
		$desc = Desc::find(1);
		$desc->DescGallery = strip_tags(Input::get('desc'));
		$desc->save();
		return Redirect::back()->with('message2','Edit Gallery description success!');
	}

	public function GetAdminGalleryDelete($id)
	{
		$gallery = Gallery::find($id);
		File::delete($gallery->GalleryImage,$gallery->GalleryImageThumb);
		$gallery->delete();
		return Redirect::back();
	}

	public function GetAdminGalleryEdit($id)
	{
		
		$gallery = Gallery::find($id);
		return View::make('admin.gallery-edit',['gallery' => $gallery]);
	}


	public function PostAdminGalleryEdit($id)
	{		
		$data = array(
				'photo'			=> Input::file('photo'),
				'GalleryName'	=> Input::get('title'),
				'subtitle'		=> Input::get('subtitle')
		);

		$validator = Validator::make($data , array(
				'photo' 		=> 'image|required',
				'subtitle'		=> 'required',
				'GalleryName'	=> 'required'
		));

		if($validator->passes())
		{
			$gallery = Gallery::find($id);

			if(Input::file('photo') != ''){
				// Validate image size
				list($width , $height) = getimagesize(Input::file('photo'));
				if($width < 360 || $height < 260) return Redirect::back()->with('message','Image is too small !!');

				File::delete($gallery->GalleryImage,$gallery->GalleryImageThumb);

				$filename 					= str_replace(' ','_',strtolower(Input::get('title'))).'.'.Input::file('photo')->getClientOriginalExtension();
				$location 					= base_path().'/asset/img/gallery/';
				Input::file('photo')		->move($location  , $filename );

				$gallery ->GalleryImage 		= 'asset/img/gallery/' . $filename;
				$gallery ->GalleryImageThumb 	= Image::thumbCustom('asset/img/gallery', $filename, 360, 260);
			}
			
			
			$gallery ->GalleryName 			= strip_tags(Input::get('title'));
			$gallery ->GalleryDesc  		= strip_tags(Input::get('subtitle'));
			
			$gallery ->save();
			return Redirect::back()->with('message','Edit gallery success!');
		}
		else
		{
			return Redirect::back()->with('message',$validator->messages()->first());;
		}
	}

	


	/*==================  SLIDER IMAGE AREA ==================*/


	public function GetAdminSliderImageView()
	{
		$slider = Slider::all();
		return View::make('admin.slider-images',['slider' => $slider]);
	}

	public function PostAdminSliderImageView()
	{
		$validator = Validator::make(Input::all() , array(
				'photo' 		=> 'image|required',
				'id'			=> 'required'
			));
		if($validator->passes())
		{
			$filename = 'slider'.Input::get('id').'.'.Input::file('photo')->getClientOriginalExtension();
			$location = base_path().'/asset/img/slider/';
			Input::file('photo')->move($location  , $filename );

			$slider 					= Slider::find(Input::get('id'));
			$slider->SliderImage 		= 'asset/img/slider/'.$filename;
			$slider->SliderTransition 	= Input::get('transition');
			$slider->save();
			return Redirect::back()->with('message','Add Slider success!');
		}
		else
		{
			return Redirect::back()->with('message','Something error, please fill up all the input form!');
		}
		
	}

	public function GetAdminSliderImageDelete($id)
	{
		$slider 					= Slider::find($id);
		$slider->SliderImage 		= '';
		$slider->SliderTransition 	= '';
		$slider->save();
		return Redirect::back();
	}


	


	/*==================  NEWS AREA ==================*/

	public function GetAdminNewsView()
	{
		$news = News::orderBy('NewsId','DESC')->paginate(10);
		return View::make('admin.news',['news' => $news]);
	}

	public function PostAdminNewsView()
	{
		$validator = Validator::make(Input::all() , array(
			'photo' 		=> 'image|required',
			'title'			=> 'required',
			'content'		=> 'required'
		));
		if($validator->passes())
		{
			$filename = str_replace(' ','_',strtolower(Input::get('title'))).'.'.Input::file('photo')->getClientOriginalExtension();
			$location = base_path().'/asset/img/news/';
			Input::file('photo')->move($location  , $filename );

			$news = new News;
			$news->NewsTittle 		= strip_tags(Input::get('title'));
			$news->NewsContent		= $this-> cleanInput(Input::get('content'));
			$news->NewsImage		= 'asset/img/news/'.$filename;
			$news->NewsImageThumb	= Image::mythumb('asset/img/news', $filename);
			$news->NewsWriter		= Auth::user()->FullName;
			$news->NewsTags 		= strip_tags(Input::get('tags'));
			$news->save();

			return Redirect::back()->with('message','Add news success!');
		}
		else
		{
			return Redirect::back()->with('message','Something wrong, please fill up all the input form!');
		}

		return View::make('admin.news');
	}

	public function GetAdminNewsDelete($id)
	{
		$news = News::find($id);
		File::delete($news->NewsImage,$news->NewsImageThumb);
		$news->delete(); 
		return Redirect::back();
	}

	public function GetAdminNewsEdit($id)
	{
		$news = News::find($id);
		return View::make('admin.news-edit',['news' => $news]);
	}

	public function PostAdminNewsEdit($id)
	{
		$validator = Validator::make(Input::all() , array(
			'photo' 		=> 'image',
			'title'			=> 'required',
			'content'		=> 'required'
		));
		if($validator->passes())
		{
			$news = News::find($id);

			if(Input::file('photo') != ''){
				File::delete($news->NewsImage,$news->NewsImageThumb);

				$filename					= str_replace(' ','_',strtolower(Input::get('title'))).'.'.Input::file('photo')->getClientOriginalExtension();
				$location 					= base_path().'/asset/img/news/';
				Input::file('photo')		->move($location  , $filename );

				$news ->NewsImage 			= 'asset/img/news/'.$filename;
				$news ->NewsImageThumb 	= Image::mythumb('asset/img/news', $filename);
			}
			
			
			$news ->NewsTittle			= strip_tags(Input::get('title'));
			$news ->NewsContent  		= $this->cleanInput(Input::get('content'));
			$news ->NewsTags 			= strip_tags(Input::get('tags'));
			
			$news ->save();
			return Redirect::back()->with('message','Edit news success!');
		}
		else
		{
			return Redirect::back()->with('message','Something wrong, please fill up all the input form!');
		}
	}


	/*==================  MEDIA AREA ==================*/

	
	public function GetAdminMediaView()
	{
		$media = Media::orderBy('MediaId','DESC')->paginate(10);
		return View::make('admin.media',['media' => $media]);
	}

	public function PostAdminMediaView()
	{
		$validator = Validator::make(Input::all() , array(
			'embed' 		=> 'required',
			'title'			=> 'required',
			'content'		=> 'required'
		));
		if($validator->passes())
		{

			
			if($this->videoType(Input::get('embed')) != 'youtube' && $this->videoType(Input::get('embed')) != 'vimeo')
			{
				return Redirect::back()->with('message','Something wrong, not a valid youtube or vimeo url!');
			}
			
			$media 					= new Media;
			$media->MediaTittle 	= strip_tags(Input::get('title'));
			$media->MediaContent	= $this-> cleanInput(Input::get('content'));
			$media->MediaVideo		= strip_tags(Input::get('embed'));;
			$media->MediaWriter		= Auth::user()->FullName;
			$media->MediaTags 		= strip_tags(Input::get('tags'));
			$media->save();

			return Redirect::back()->with('message','Add media success!');
		}
		else
		{
			return Redirect::back()->with('message','Something wrong, please fill up all the input form!');
		}

	}

	public function GetAdminMediaDelete($id)
	{
		$media = Media::find($id);
		$media->delete(); 
		return Redirect::back();
	}

	public function GetAdminMediaEdit($id)
	{
		$media = Media::find($id);
		return View::make('admin.media-edit',['media' => $media]);
	}

	public function PostAdminMediaEdit($id)
	{
		$validator = Validator::make(Input::all() , array(
			'embed' 		=> 'required',
			'title'			=> 'required',
			'content'		=> 'required'
		));
		if($validator->passes())
		{
			if($this->videoType(Input::get('embed')) != 'youtube' && $this->videoType(Input::get('embed')) != 'vimeo')
			{
				return Redirect::back()->with('message','Something wrong, not a valid youtube or vimeo url!');
			}

			$media 					= Media::find($id);
			$media->MediaTittle 	= strip_tags(Input::get('title'));
			$media->MediaContent	= $this-> cleanInput(Input::get('content'));
			$media->MediaVideo		= strip_tags(Input::get('embed'));
			$media->MediaWriter		= Auth::user()->FullName;
			$media->MediaTags 		= strip_tags(Input::get('tags'));
			$media->save();
			return Redirect::back()->with('message','Edit media success!');
		}
		else
		{
			return Redirect::back()->with('message','Something wrong, please fill up all the input form!');
		}
	}



	/*==================  USER PROFILE AREA ==================*/

	public function GetAdminUserProfileView($id)
	{
		if(Auth::user()->UserLevel > 1 && $id != Auth::user()->UserId) return Redirect::to('adm/dashboard');

		$user = Users::find($id);
		return View::make('admin.user-profile',['user' => $user]);
	}

	public function PostAdminUserProfileView($id)
	{
		$input = array(
				'photo' 		=> Input::file('photo'),
				'username'		=> Input::get('username'),
				'password' 		=> Input::get('password'),
				'repassword' 	=> Input::get('repassword'),
				'email'			=> Input::get('email')
			);

		$rules = array(
				'photo' 		=> 'image',
				'username'		=> 'max:15',
				'password' 		=> 'max:30',
				'repassword' 	=> 'same:password',
				'email'			=> 'email'
			);
		$validator = Validator::make($input,$rules);

		if($validator->passes())
		{
			$user     = Users::find($id);
			$filename = '';
			$fileurl  = '';

			if(Input::file('photo')!='')
			{
				$filename = $user->Username.'.'.Input::file('photo')->getClientOriginalExtension();
				$location = base_path().'/asset2/img/ava/';
				Input::file('photo')->move($location  , $filename );
			}
			
			if($filename != '') $fileurl = 'asset2/img/ava/'.$filename;

			if(Input::get('password')!='')$user->Password = Hash::make(Input::get('password'));
			
			if(Auth::user()->UserLevel == 1){ 
			$user->UserLevel 	= Input::get('userlevel');}
			$user->Username 	= $this->cleanInput(Input::get('username'));
			$user->Email 		= $this->cleanInput(Input::get('email'));
			$user->FullName 	= $this->cleanInput(Input::get('fullname'));
			$user->Gender 		= $this->cleanInput(Input::get('gender'));
			$user->signature	= $this->cleanInput(Input::get('signature'));
			$user->UserPhoto	= $fileurl;
			$user->save();
			return Redirect::back()->with('message','Profile edit success!');
		}
		else
		{
			$msg = $validator->messages();
			return Redirect::back()->with('message','Something error!');
		}
	}

	public function GetAdminUserProfileDelete($id)
	{
		if(Auth::user()->UserLevel > 1) return Redirect::to('adm/dashboard');

		$user = Users::find($id);
		try{
		File::delete($user->UserPhoto);
		}catch(exception $e)
		{

		}
		$user->delete();
		return Redirect::back();
	}

	/*==================  USER MANAGEMENT PAGE AREA ==================*/

	public function GetAdminUserManagementView()
	{
		$userlist = Users::orderBy('UserId','asc')->paginate(10);	
		return View::make('admin.user-management', ['userlist' => $userlist]);
	}

	public function PostAdminUserManagementView()
	{
		$data = array(
			'photo' 		=> Input::file('photo'),
			'Username'		=> Input::get('username'),
			'fullname'		=> Input::get('fullname'),
			'email'			=> Input::get('email'),
			'password'		=> Input::get('password'),
			'repassword'	=> Input::get('repassword'),
			'gender'		=> Input::get('gender'),
		);
		$validator = Validator::make($data , array(
			'photo' 		=> 'image',
			'Username'		=> 'required|unique:user',
			'fullname'		=> 'required',
			'email'			=> 'required|email',
			'password'		=> 'required|min:6',
			'repassword'	=> 'required|same:password',
			'gender'		=> 'required'
		));
		if($validator->passes())
		{
			$user 		= new Users;
			$username  	= $this->cleanInput(Input::get('username'));
			if(Input::file('photo') != '')
			{
				$filename					= $username.'.'.Input::file('photo')->getClientOriginalExtension();
				$location 					= base_path().'/asset2/img/ava/';
				Input::file('photo')		->move($location  , $filename );	
				$user->UserPhoto	 		= 'asset2/img/ava/'.$filename;
			}
			$user->Password 				= $this-> cleanInput(Hash::make(Input::get('password')));
			$user->FullName 				= $this-> cleanInput(Input::get('fullname'));
			$user->Username 				= $this-> cleanInput(Input::get('username'));
			$user->Email 					= $this-> cleanInput(Input::get('email'));
			$user->Signature 				= $this-> cleanInput(Input::get('signature'));
			$user->Gender 					= Input::get('gender');
			$user->UserLevel 				= Input::get('level');
			$user->save();
			return Redirect::back()->with('message','Add user success!');
		}
		else
		{
			return Redirect::back()->with('message',$validator->messages()->first())->withInput(Input::except('password'));;
		}
	}

	/*==================  OPENING PAGE AREA ==================*/

	public function GetAdminOpeningPageView()
	{
		$url = Opening::find(1);
		if($url == ''){
			$data 				= new Opening;
			$data->OpeningId 	= 1;
			$data->OpeningPhoto = "asset2/img/dummy_150x150.gif";
			$data->save();
			$url = Opening::find(1);
		}
		
		return View::make('admin.opening-page',['opening' => $url]);
	}

	public function PostAdminOpeningPageView()
	{
		$input = array(
			'photo' => Input::file('photo')
		);

		$rules = array(
			'photo' => 'image'
		);

		$validator = Validator::make($input,$rules);
		if($validator->passes())
		{
			$opening 					= Opening::find(1);
			$filename 					= 'opening-background.'.Input::file('photo')->getClientOriginalExtension();
			$location 					= base_path().'/asset/img/openingpage/';
			Input::file('photo')		-> move($location  , $filename );
			$opening->OpeningPhoto 		= 'asset/img/openingpage/'.$filename;
			$opening->save();
			return Redirect::back();
		}
		else
		{
			return Redirect::back();
		}
	}

	/*==================  ABOUT AREA ==================*/


	public function GetAdminAboutView()
	{
		$about 		= About::find(1);
		$partner 	= Partner::all();
		return View::make('admin.about', ['about' => $about, 'partner' => $partner]);
	}

	public function PostAdminAboutView()
	{	
		$validator = Validator::make(Input::all() , array(
			'photo' 		=> 'image'
		));

		if($validator->passes())
		{
			$about = About::find(1);

			if(Input::file('photo') != '')
			{
				$filename					= 'company.'.Input::file('photo')->getClientOriginalExtension();
				$location 					= base_path().'/asset/img/about/';
				Input::file('photo')		->move($location  , $filename );	
				$about->AboutImage	 		= 'asset/img/about/'.$filename;
			}
			$about->AboutContent 			= $this-> cleanInput(Input::get('content'));
			$about->AboutContentVision 		= $this-> cleanInput(Input::get('vision'));
			$about->AboutContentMission 	= $this-> cleanInput(Input::get('mission'));
			$about->save();
			return Redirect::back();
		}
		else
		{
			return Redirect::back()->with('message','Invalid image!');
		}
	}

	public function PostAdminAboutPartnerView()
	{	
		$validator = Validator::make(Input::all() , array(
			'photo' 	=> 'image|required',
			'id'		=> 'required'
		));

		if($validator->passes())
		{
			$partner = Partner::find(Input::get('id'));
			$filename						= 'partner'.Input::get('id').'.'.Input::file('photo')->getClientOriginalExtension();
			$location 						= base_path().'/asset/img/about/';
			Input::file('photo')			->move($location  , $filename );	
			$partner->PartnerImage	 		= 'asset/img/about/'.$filename;
			$partner->save();
			return Redirect::back();
		}
		else
		{
			return Redirect::back()->with('message','Invalid image!');
		}
	}

	public function GetAdminAboutPartnerDelete($id)
	{
		$partner 					= Partner::find($id);
		$partner ->PartnerImage 	= '';
		$partner ->save();
		return Redirect::back();
	}

	/*==================  SITE BANNER AREA ==================*/	

	public function GetAdminBannerView()
	{
		$desc = Desc::find(1);
		return View::make('admin.banner', ['desc' => $desc]);
	}



	public function GetAdminBannerHeaderPost()
	{
		$validator = Validator::make(Input::all() , array(
			'photo' 		=> 'image',
			'title'			=> 'required',
			'desc'			=> 'required',
			'email'			=> 'email',
			'welcome'		=> 'required'
			//'phone'			=> 'required'

		));

		if($validator->passes())
		{
			$desc = Desc::find(1);

			if(Input::file('photo') != '')
			{
				$filename					= 'banner-header.'.Input::file('photo')->getClientOriginalExtension();
				$location 					= base_path().'/asset/img/';
				Input::file('photo')		->move($location  , $filename );	
				$desc->DescBannerImage	 	= 'asset/img/'.$filename;
			}
			
			$desc->DescBannerTitle  	= $this->cleanInput(Input::get('title'));
			$desc->DescBannerSubtitle 	= $this->cleanInput(Input::get('desc'));
			$desc->DescBannerEmail		= $this->cleanInput(Input::get('email'));
			$desc->DescBannerPhone		= $this->cleanInput(Input::get('phone'));
			$desc->DescWelcome			= $this->cleanInput(Input::get('welcome'));
			$desc->save();
			
			return Redirect::back()->with('head','Success!');;
		}
		else
		{
			return Redirect::back()->with('head',$validator->messages()->first());
		}
	}

	public function GetAdminBannerBodyPost()
	{
		$validator = Validator::make(Input::all() , array(
			'photo' 		=> 'image',
			'desc'			=> 'required'
		));

		if($validator->passes())
		{
			$desc = Desc::find(1);

			if(Input::file('photo') != '')
			{
				$filename					= 'banner-body.'.Input::file('photo')->getClientOriginalExtension();
				$location 					= base_path().'/asset/img/';
				Input::file('photo')		->move($location  , $filename );	
				$desc->DescBodyBannerImage	= 'asset/img/'.$filename;
			}
			
			$desc->DescBodyBannerTitle		= $this->cleanInput(Input::get('desc'));
			$desc->save();
			
			return Redirect::back()->with('body','Success!');;
		}
		else
		{
			return Redirect::back()->with('body',$validator->messages()->first());
		}
	}

	public function GetAdminBannerFooterPost()
	{
		$validator = Validator::make(Input::all() , array(
			'content' 		=> 'required'

		));

		if($validator->passes())
		{
			$desc = Desc::find(1);

			$desc->DescFooterBannerAddress  	= $this->cleanInput(Input::get('content'));
			$desc->save();
			
			return Redirect::back()->with('footer','Success!');;
		}
		else
		{
			return Redirect::back()->with('footer',$validator->messages()->first());
		}
	}

	/*==================  SITE SERVICES AREA ==================*/	

	public function GetAdminServicesView()
	{
		$desc = Desc::find(1);
		return View::make('admin.services', ['desc' => $desc]);
	}

	public function GetAdminServicesPost()
	{
		$validator = Validator::make(Input::all() , array(
			'content' 		=> 'required'

		));

		if($validator->passes())
		{
			$desc = Desc::find(1);

			$desc->DescFooterBannerAddress  	= $this->cleanInput(Input::get('content'));
			$desc->save();
			
			return Redirect::back()->with('footer','Success!');;
		}
		else
		{
			return Redirect::back()->with('footer',$validator->messages()->first());
		}
	}	

	/*==================  SITE SETTING AREA ==================*/	

	public function GetAdminSiteSettingView()
	{
		$setting = Setting::find(1);
		return View::make('admin.site-setting',['setting' => $setting]);
	}

	public function PostAdminSiteSettingView()
	{
		if(Input::get('opening') == 'on') $opening = 1; else $opening = 0;
		$setting = Setting::find(1);
		$setting->SettingSiteName 		= $this->cleanInput(Input::get('name'));
		$setting->SettingMeta 			= $this->cleanInput(Input::get('meta'));
		$setting->SettingOpeningPage	= $opening;
		$setting->save();
		return Redirect::back();
	}

	/*==================  CUSTOM AREA ==================*/
/*
	public function _Hash($word)
	{
		$hash1 = Hash::make($word);
		$hash2 = Hash::make($word);
		echo $hash1.'<br>';
		echo $hash2.'<br>';
		var_dump(Hash::check($word, $hash2));
	}
*/

}
