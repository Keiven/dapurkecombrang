<?php

	class Partner extends Eloquent{
		protected $table = 'partner';
		protected $primaryKey = 'PartnerId';
		public $timestamps = false;
	}
