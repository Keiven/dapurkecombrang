<?php

	class Analytic extends Eloquent{
		protected $table = 'analytics';
		protected $primaryKey = 'AnalyticId';
		public $timestamps = true;
	}