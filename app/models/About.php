<?php

	class About extends Eloquent{
		protected $table = 'about';
		protected $primaryKey = 'AboutId';
		public $timestamps = false;
	}
