<?php

	class Desc extends Eloquent{
		protected $table = 'desc';
		protected $primaryKey = 'DescId';
		public $timestamps = false;
	}
