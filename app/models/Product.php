<?php

	class Product extends Eloquent{
		protected $table = 'product';
		protected $primaryKey = 'ProductId';
		public $timestamps = true;
	}