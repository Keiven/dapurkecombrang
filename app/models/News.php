<?php

	class News extends Eloquent{
		protected $table = 'news';
		protected $primaryKey = 'NewsId';
		public $timestamps = true;
	}