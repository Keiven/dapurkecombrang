<?php

	class Setting extends Eloquent{
		protected $table = 'setting';
		protected $primaryKey = 'SettingId';
		public $timestamps = false;
	}