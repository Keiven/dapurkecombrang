<?php

	class Category extends Eloquent{
		protected $table = 'product_category';
		protected $primaryKey = 'CategoryName';
		public $timestamps = false;
	}