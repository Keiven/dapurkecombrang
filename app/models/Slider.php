<?php

	class Slider extends Eloquent{
		protected $table = 'slider';
		protected $primaryKey = 'SliderId';
		public $timestamps = false;
	}
