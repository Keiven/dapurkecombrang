<?php

	class Opening extends Eloquent{
		protected $table = 'opening';
		protected $primaryKey = 'OpeningId';
		public $timestamps = false;
	}