<?php

	class Media extends Eloquent{
		protected $table = 'media';
		protected $primaryKey = 'MediaId';
		public $timestamps = true;
	}