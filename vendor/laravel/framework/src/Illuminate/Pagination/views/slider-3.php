<?php
    $presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);

    $prevUrl = $paginator->getUrl($paginator->getCurrentPage() - 1);

    $nextPage = $paginator->getCurrentPage() + 1;
    if($nextPage > $paginator->getLastPage())
    {
        $nextPage = $paginator->getCurrentPage();
    }
    $nextUrl = $paginator->getUrl($nextPage);

?>


<?php if ($paginator->getLastPage() > 1): ?>
<li class="pagination" style="list-style-type: none;">
	<a class="pagination-prev" href="<?php echo $prevUrl;?>">&#60; Previous</a>

	<div class="pager">
	    <ul>
	        <?php echo $presenter->render(); ?>
	    </ul>
	</div>

	<a class="pagination-next" href="<?php echo $nextUrl;?>">Next &#62;</a>
</li>
<?php endif; ?>